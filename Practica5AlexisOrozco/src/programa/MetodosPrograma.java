package programa;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.Scanner;

import clases.Biblioteca;
import clases.Cuento;
import clases.LibroRecetas;
import clases.Novela;
import clases.Socio;

public class MetodosPrograma {
	static Scanner input = new Scanner(System.in);
	static int opcion;
	static String respuesta;
	static boolean repetir;

	/**
	 * Metodo para volver a intentar una introduccion de datos
	 */
	private static void intentarNuevamente() {
		do {
			System.out.println("Volver a intentarlo S/N");
			respuesta = input.nextLine();
			if (respuesta.equalsIgnoreCase("s")) {
				repetir = true;
			} else if (respuesta.equalsIgnoreCase("n")) {
				repetir = false;
			} else {
				System.out.println("Solo se admite las respuesta s / n");
			}
		} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
	}

	/**
	 * Metodo para comprobar si un dato es un numero
	 * 
	 * @param numero
	 * @return
	 */
	private static int comprobarNumero() {
		int numero = 0;
		boolean error = true;
		while (error == true) {
			error = false;
			try {
				numero = input.nextInt();
			} catch (java.util.InputMismatchException e) {
				System.out.println("No es un n�mero " + e);
				System.out.println("Vuelvelo a intentar");
				input.nextLine();
				error = true;
			}
		}
		return numero;
	}

	/**
	 * Metodo para cerrar el Scanner y cerrar el programa
	 */
	private static void fin() {
		input.close();
		System.exit(0);
	}

	// ************************************************************************************************
	// MENU PRINCIPAL
	// ************************************************************************************************
	/**
	 * Metodo que llama al men� principal del programa
	 * 
	 * @param biblioteca05
	 */
	static void menuPrincipal(Biblioteca biblioteca05) {
		do {
			Datos.dibujoLibro();
			Datos.textoMenuPrincipal();
			opcion = comprobarNumero();
			input.nextLine();
			switch (opcion) {

			case 1:
				menuNovela(biblioteca05);
				break;
			case 2:
				menuCuento(biblioteca05);
				break;
			case 3:
				menuRecetario(biblioteca05);
				break;
			case 4:
				menuSocio(biblioteca05);
				break;
			case 5:
				System.out.println("Fin del programa");
				Datos.dibujoLibro();
				fin();
				break;

			default:
				System.out.println("*************************");
				System.out.println("* Opci�n no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}

		} while (opcion != 5);
	}

	// ************************************************************************************************
	// MENU NOVELA
	// ************************************************************************************************
	/**
	 * Metodo que llama al menu de novelas
	 * 
	 * @param biblioteca05
	 */
	static void menuNovela(Biblioteca biblioteca05) {
		do {
			Novela novela = null;
			Datos.textoMenuNovela();
			opcion = comprobarNumero();

			input.nextLine();
			switch (opcion) {

			case 1:
				do {
					biblioteca05.aniadirNovela(biblioteca05);
					do {
						System.out.println("Desea a�adir otro S/N");
						respuesta = input.nextLine();
						if (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n")) {
							System.out.println("Solo se admiten las respuetas s / n");
						}
					} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
				} while (respuesta.equalsIgnoreCase("s"));
				break;
			case 2:
				biblioteca05.listarNovelas();
				break;
			case 3:
				System.out.println(buscarNovela(biblioteca05));
				break;
			case 4:
				novela = null;
				novela = buscarNovela(biblioteca05);
				novela.valoracionDelPublico();
				novela.mostrarDatos();
				break;
			case 5:
				cogerNovela(biblioteca05);
				break;
			case 6:
				devolverNovela(biblioteca05);
				break;
			case 7:
				eliminarNovela(biblioteca05);
				break;
			case 8:
				System.out.println("Volver al menu principal");
				break;

			default:
				System.out.println("*************************");
				System.out.println("* Opci�n no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}

		} while (opcion != 8);
	}

	/**
	 * Metodo para eliminar una novela
	 * 
	 * @param biblioteca05
	 */
	private static void eliminarNovela(Biblioteca biblioteca05) {
		Novela novela;
		novela = null;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el codigo de la novela a eliminar");
			novela = biblioteca05.buscarNovelaCodigo(input.nextLine());
			if (novela != null) {
				biblioteca05.novelaDescatalogada(novela.getCodigo());
				repetir = false;
			} else {
				System.out.println("La novela no existe");
				intentarNuevamente();
			}
		}
	}

	/**
	 * Metodo para devolveer una novela a la biblioteca
	 * 
	 * @param biblioteca05
	 */
	private static void devolverNovela(Biblioteca biblioteca05) {
		Novela novela;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el codigo del Libro");
			novela = biblioteca05.buscarNovelaCodigo(input.nextLine());
			if (novela != null) {
				if (novela.isEstado() == true) {
					System.out.println("");
					novela.mostrarDatos();
					System.out.println(" ");

					LocalDate fecha = novela.getFechaDevolucion();
					if (fecha.isBefore(LocalDate.now()) == true) {
						System.out.println("Has devuelto el Libro despues de la fecha indicada");
						Socio socio = novela.getSocio();
						socio.obtenerMultaTotalRetraso(biblioteca05, fecha);
						socio.mostrarDatos();
						System.out.println(" ");
					}

					do {
						System.out.println("Confiesa tu madre te ha dicho que ese libro es para gente m�s mayor s/n");
						respuesta = input.nextLine();
						if (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n")) {
							System.out.println("Solo se admiten las respuetas s / n");
						}
					} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
					if (respuesta.equalsIgnoreCase("s")) {
						novela.aumentarEdad();
					}

					novela.devolverLibro();
				} else {
					System.out.println("La novela ya est� en la biblioteca");
				}
				System.out.println(" ");
				novela.mostrarDatos();
				System.out.println(novela);
				repetir = false;
			} else {
				System.out.println("La novela no existe");
				intentarNuevamente();
			}
		}
	}

	/**
	 * Metodo para coger una novela prestada de la biblioteca
	 * 
	 * @param biblioteca05
	 */
	private static void cogerNovela(Biblioteca biblioteca05) {
		Novela novela;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el numero de socio");
			opcion = comprobarNumero();
			Socio socio = biblioteca05.buscarSocio(opcion);
			input.nextLine();
			System.out.println("Introduce el codigo del Libro");
			novela = biblioteca05.buscarNovelaCodigo(input.nextLine());
			if (novela == null && socio == null) {
				System.out.println("Ambos datos son erroneos");
				intentarNuevamente();
			} else if (novela == null) {
				System.out.println("La novela no existe");
				intentarNuevamente();
			} else if (socio == null) {
				System.out.println("El socio no existe");
				intentarNuevamente();
			} else {
				novela.cogerLibroPrestado(socio);
				novela.mostrarDatos();
				repetir = false;
			}
		}
	}

	/**
	 * Metodo para buscar una novela
	 * 
	 * @param biblioteca05
	 * @return - devuelve la novela
	 */
	private static Novela buscarNovela(Biblioteca biblioteca05) {
		Novela novela = null;
		do {
			System.out.println("Como quieres buscar el Libro por Titulo(1) o por Codigo(2)");
			opcion = comprobarNumero();
			input.nextLine();
			if (opcion == 1) {
				System.out.println("Da el Titulo a Buscar");
				novela = biblioteca05.buscarNovelaTitulo(input.nextLine());// Exception
																			// nullpointer???????????????????????????
				if (novela == null) {
					System.out.println("La novela no existe");
					intentarNuevamente();
				} else {
					return novela;
				}
			} else if (opcion == 2) {

				System.out.println("Da el Codigo a Buscar");
				novela = biblioteca05.buscarNovelaCodigo(input.nextLine());// Exception
																			// nullpointer???????????????????????????
				if (novela == null) {
					System.out.println("La novela no existe");
					intentarNuevamente();
				} else {
					return novela;
				}
			} else {
				System.out.println("Elige Titulo(1) o Codigo(2)");

			}
		} while (opcion != 1 && opcion != 2 || novela == null);
		return null;
	}

	// ************************************************************************************************
	// MENU CUENTO
	// ************************************************************************************************
	/**
	 * metodo que llama al menu de cuentos
	 * 
	 * @param biblioteca05
	 */
	static void menuCuento(Biblioteca biblioteca05) {
		do {
			Cuento cuento = null;
			Datos.textoMenuCuento();
			opcion = comprobarNumero();

			input.nextLine();
			switch (opcion) {

			case 1:
				do {
					biblioteca05.aniadirCuento(biblioteca05);
					do {
						System.out.println("Desea a�adir otro S/N");
						respuesta = input.nextLine();
						if (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n")) {
							System.out.println("Solo se admiten las respuetas s / n");
						}
					} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
				} while (respuesta.equalsIgnoreCase("s"));
				break;
			case 2:
				biblioteca05.listarCuentos();
				break;
			case 3:
				System.out.println(buscarCuento(biblioteca05));
				break;
			case 4:
				cuento = null;
				cuento = buscarCuento(biblioteca05);
				cuento.valoracionDelPublico();
				cuento.mostrarDatos();
				break;
			case 5:
				cogerCuento(biblioteca05);
				break;
			case 6:
				devolverCuento(biblioteca05);

				break;
			case 7:
				eliminarCuento(biblioteca05);
				break;
			case 8:
				System.out.println("Volver al menu principal");
				break;

			default:
				System.out.println("*************************");
				System.out.println("* Opci�n no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}

		} while (opcion != 8);
	}

	/**
	 * Metodo para buscar un Cuento
	 * 
	 * @param biblioteca05
	 * @return - devuelve el cuento
	 */
	private static Cuento buscarCuento(Biblioteca biblioteca05) {
		Cuento cuento = null;
		do {
			System.out.println("Como quieres buscar el Libro por Titulo(1) o por Codigo(2)");
			opcion = comprobarNumero();
			input.nextLine();
			if (opcion == 1) {
				System.out.println("Da el Titulo a Buscar");
				cuento = biblioteca05.buscarCuentoTitulo(input.nextLine());// Exception
																			// nullpointer???????????????????????????
				if (cuento == null) {
					System.out.println("El cuento no existe");
					intentarNuevamente();
				} else {
					return cuento;
				}
			} else if (opcion == 2) {

				System.out.println("Da el Codigo a Buscar");
				cuento = biblioteca05.buscarCuentoCodigo(input.nextLine());// Exception
																			// nullpointer???????????????????????????
				if (cuento == null) {
					System.out.println("El cuento no existe");
					intentarNuevamente();
				} else {
					return cuento;
				}
			} else {
				System.out.println("Elige Titulo(1) o Codigo(2)");

			}
		} while (opcion != 1 && opcion != 2 || cuento == null);
		return null;
	}

	/**
	 * Metodo para coger un cuento prestado de la biblioteca
	 * 
	 * @param biblioteca05
	 */
	private static void cogerCuento(Biblioteca biblioteca05) {
		Cuento cuento;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el numero de socio");
			opcion = comprobarNumero();
			Socio socio = biblioteca05.buscarSocio(opcion);
			input.nextLine();
			System.out.println("Introduce el codigo del Libro");
			cuento = biblioteca05.buscarCuentoCodigo(input.nextLine());
			if (cuento == null && socio == null) {
				System.out.println("Ambos datos son erroneos");
				intentarNuevamente();
			} else if (cuento == null) {
				System.out.println("El cuento no existe");
				intentarNuevamente();
			} else if (socio == null) {
				System.out.println("El socio no existe");
				intentarNuevamente();
			} else {
				cuento.cogerLibroPrestado(socio);
				cuento.mostrarDatos();
				repetir = false;
			}
		}
	}

	/**
	 * Metodo para devolver un cuento a la biblioteca
	 * 
	 * @param biblioteca05
	 */
	private static void devolverCuento(Biblioteca biblioteca05) {
		Cuento cuento;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el codigo del Libro");
			cuento = biblioteca05.buscarCuentoCodigo(input.nextLine());
			if (cuento != null) {
				if (cuento.isEstado() == true) {
					System.out.println(" ");
					cuento.mostrarDatos();
					System.out.println(" ");

					LocalDate fecha = cuento.getFechaDevolucion();
					if (fecha.isBefore(LocalDate.now()) == true) {
						System.out.println("Has entregado el libro despues de la fecha asignada");
						Socio socio = cuento.getSocio();
						socio.obtenerMultaTotalRetraso(biblioteca05, fecha);
						socio.mostrarDatos();
						System.out.println(" ");
					}

					boolean error = false;
					int paginas = 0;
					do {
						System.out.println("Cu�ntas p�ginas ha arrancado el ni�o");
						try {
							paginas = input.nextInt();
							input.nextLine();
						} catch (InputMismatchException e) {
							input.nextLine();
							error = true;
							System.out.println("No es un numero " + e);
						}
					} while (error == true);
					cuento.quitarPaginas(paginas);
					cuento.devolverLibro();
				} else {
					System.out.println("El cuento ya est� en la biblioteca");
				}
				System.out.println(" ");
				cuento.mostrarDatos();
				System.out.println(cuento);
				repetir = false;
			} else {
				System.out.println("El cuento no existe");
				intentarNuevamente();
			}
		}
	}

	/**
	 * Metodo para eliminar una novela
	 * 
	 * @param biblioteca05
	 */
	private static void eliminarCuento(Biblioteca biblioteca05) {
		Cuento cuento;
		cuento = null;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el codigo del cuento a eliminar");
			cuento = biblioteca05.buscarCuentoCodigo(input.nextLine());
			if (cuento != null) {
				biblioteca05.cuentoDescatalogado(cuento.getCodigo());
				repetir = false;
			} else {
				System.out.println("El cuento no existe");
				intentarNuevamente();
			}
		}
	}

	// ************************************************************************************************
	// MENU LIBRO RECETAS
	// ************************************************************************************************
	/**
	 * metodo que llama al menu de libros de recetas
	 * 
	 * @param biblioteca05
	 */
	static void menuRecetario(Biblioteca biblioteca05) {
		do {
			LibroRecetas recetario = null;
			Datos.textoMenuLibroRecetas();
			opcion = comprobarNumero();

			input.nextLine();
			switch (opcion) {

			case 1:
				do {
					nuevoLibroRecetas(biblioteca05);
					do {
						System.out.println("Desea a�adir otro S/N");
						respuesta = input.nextLine();
						if (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n")) {
							System.out.println("Solo se admiten las respuetas s / n");
						}
					} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
				} while (respuesta.equalsIgnoreCase("s"));
				break;
			case 2:
				do {
					System.out.println("Desea ver las recetas por libro S/N");
					respuesta = input.nextLine();
					if (respuesta.equalsIgnoreCase("s")) {
						biblioteca05.listarLibroRecetas2();
					} else if (respuesta.equalsIgnoreCase("n")) {
						biblioteca05.listarLibroRecetas();
					} else {
						System.out.println("Solo se admite las respuesta s / n");
					}
				} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
				break;
			case 3:
				System.out.println(buscarLibroRecetas(biblioteca05));
				break;
			case 4:
				recetario = null;
				recetario = buscarLibroRecetas(biblioteca05);
				recetario.valoracionDelPublico();
				recetario.mostrarDatos();
				break;
			case 5:
				cogerLibroRecetas(biblioteca05);
				break;
			case 6:
				devolverLibroRecetas(biblioteca05);
				break;
			case 7:
				eliminarLibroRecetas(biblioteca05);
				break;
			case 8:
				System.out.println("Volver al menu principal");
				break;

			default:
				System.out.println("*************************");
				System.out.println("* Opci�n no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}

		} while (opcion != 8);
	}

	/**
	 * Metodo para aniadir libros de recetas de las diferentes formas posibles
	 * 
	 * @param biblioteca05
	 */
	private static void nuevoLibroRecetas(Biblioteca biblioteca05) {
		LibroRecetas libroRecetas = biblioteca05.aniadirLibroRecetas(biblioteca05);
		do {
			System.out.println("Desea a�adir una receta a este libro S/N");
			respuesta = input.nextLine();
			if (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n")) {
				System.out.println("Solo se admiten las respuetas s / n");
			}
		} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));

		while (respuesta.equalsIgnoreCase("s")) {
			libroRecetas.aniadirReceta();
			do {
				System.out.println("Desea a�adir otra receta a este libro S/N");
				respuesta = input.nextLine();

				if (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n")) {
					System.out.println("Solo se admiten las respuetas s / n");
				}
			} while (!respuesta.equalsIgnoreCase("s") && !respuesta.equalsIgnoreCase("n"));
		}
	}

	/**
	 * Metodo para buscar un Libro de Recetas
	 * 
	 * @param biblioteca05
	 * @return - devuelve el Libro de Recetas
	 */
	private static LibroRecetas buscarLibroRecetas(Biblioteca biblioteca05) {
		LibroRecetas recetario = null;
		do {
			System.out.println("Como quieres buscar el Libro por Titulo(1) o por Codigo(2)");
			opcion = comprobarNumero();
			input.nextLine();
			if (opcion == 1) {
				System.out.println("Da el Titulo a Buscar");
				recetario = biblioteca05.buscarRecetarioTitulo(input.nextLine());
				if (recetario == null) {
					System.out.println("El Libro de Recetas no existe");
					intentarNuevamente();
				} else {
					return recetario;
				}
			} else if (opcion == 2) {

				System.out.println("Da el Codigo a Buscar");
				recetario = biblioteca05.buscarRecetarioCodigo(input.nextLine());
				if (recetario == null) {
					System.out.println("El Libro de Recetas no existe");
					intentarNuevamente();
				} else {
					return recetario;
				}
			} else {
				System.out.println("Elige Titulo(1) o Codigo(2)");

			}
		} while (opcion != 1 && opcion != 2 || recetario == null);
		return null;
	}

	/**
	 * Metodo para coger un libro de recetas prestado de la biblioteca
	 * 
	 * @param biblioteca05
	 */
	private static void cogerLibroRecetas(Biblioteca biblioteca05) {
		LibroRecetas recetario;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el numero de socio");
			opcion = comprobarNumero();
			Socio socio = biblioteca05.buscarSocio(opcion);
			input.nextLine();
			System.out.println("Introduce el codigo del Libro");
			recetario = biblioteca05.buscarRecetarioCodigo(input.nextLine());
			if (recetario == null && socio == null) {
				System.out.println("Ambos datos son erroneos");
				intentarNuevamente();
			} else if (recetario == null) {
				System.out.println("El libro de recetas no existe");
				intentarNuevamente();
			} else if (socio == null) {
				System.out.println("El socio no existe");
				intentarNuevamente();
			} else {
				recetario.cogerLibroPrestado(socio);
				recetario.mostrarDatos();
				repetir = false;
			}
		}
	}

	/**
	 * Metodo para devolver un libro de recetas a la biblioteca
	 * 
	 * @param biblioteca05
	 */
	private static void devolverLibroRecetas(Biblioteca biblioteca05) {
		LibroRecetas recetario;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el codigo del Libro");
			recetario = biblioteca05.buscarRecetarioCodigo(input.nextLine());
			if (recetario != null) {
				if (recetario.isEstado() == true) {
					LocalDate fecha = recetario.getFechaDevolucion();
					if (fecha.isBefore(LocalDate.now()) == true) {
						Socio socio = recetario.getSocio();
						System.out.println("Has devuelto el libro despues de la fecha asignada");
						socio.obtenerMultaTotalRetraso(biblioteca05, fecha);
						socio.mostrarDatos();
						System.out.println(" ");
						recetario.devolverLibro();
					}
					System.out.println(" ");
					recetario.mostrarDatos();
					System.out.println(" ");
				} else {
					System.out.println("El Libro de recetas ya est� en la biblioteca");
				}
				System.out.println(" ");
				System.out.println(recetario);
				repetir = false;
			} else {
				System.out.println("El Libro de Recetas no existe");
				intentarNuevamente();
			}
		}
	}

	/**
	 * Metodo para eliminar un Libro de recetas
	 * 
	 * @param biblioteca05
	 */
	private static void eliminarLibroRecetas(Biblioteca biblioteca05) {
		LibroRecetas recetario;
		recetario = null;
		repetir = true;
		while (repetir != false) {
			System.out.println("Introduce el codigo del cuento a eliminar");
			recetario = biblioteca05.buscarRecetarioCodigo(input.nextLine());
			if (recetario != null) {
				biblioteca05.recetarioDescatalogado(recetario.getCodigo());
				repetir = false;
			} else {
				System.out.println("El libro de recetas no existe");
				intentarNuevamente();
			}
		}
	}

	// ************************************************************************************************
	// MENU SOCIO
	// ************************************************************************************************
	/**
	 * metodo que llama al menu de socio
	 * 
	 * @param biblioteca05
	 */
	static void menuSocio(Biblioteca biblioteca05) {
		do {
			Datos.textoMenuSocio();
			opcion = comprobarNumero();

			input.nextLine();
			switch (opcion) {

			case 1:
				biblioteca05.altaSocio();
				break;
			case 2:
				biblioteca05.listarSocios();
				break;
			case 3:
				buscarSocio(biblioteca05);
				break;
			case 4:
				librosSocio(biblioteca05);
				break;
			case 5:
				eliminarSocio(biblioteca05);
				break;
			case 6:
				System.out.println("Fin del programa");
				break;

			default:
				System.out.println("*************************");
				System.out.println("* Opci�n no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}

		} while (opcion != 6);
	}

	/**
	 * Metodo para eliminar un Socio
	 * 
	 * @param biblioteca05
	 */
	private static void eliminarSocio(Biblioteca biblioteca05) {
		Socio socio;
		socio = null;
		repetir = true;
		while (repetir != false) {
			boolean error = false;
			do {
				error = false;
				System.out.println("Da el numero de Socio a Buscar");
				try {
					socio = biblioteca05.buscarSocio(input.nextInt());
					input.nextLine();
				} catch (InputMismatchException e) {
					System.out.println("No es un numero " + e);
					input.nextLine();
					error = true;
				}
			} while (error == true);
			if (socio != null) {
				biblioteca05.darBajaSocio(socio.getNumeroSocio());
				repetir = false;
			} else {
				System.out.println("El socio no existe");
				intentarNuevamente();
			}
		}
	}

	/**
	 * Metodo que devuelve todos los libros que tenga un socio en prestamo
	 * 
	 * @param biblioteca05
	 */
	private static void librosSocio(Biblioteca biblioteca05) {
		boolean error = false;
		Socio socio = null;

		do {
			error = false;
			System.out.println("Da el numero de Socio del que quieres ver que libros tiene");
			try {
				socio = biblioteca05.buscarSocio(input.nextInt());
				input.nextLine();
			} catch (InputMismatchException e) {
				System.out.println("No es un numero " + e);
				input.nextLine();
				error = true;
			}
		} while (error == true);
		if (socio == null) {
			System.out.println("El Socio no existe");
			intentarNuevamente();
		} else {
			biblioteca05.listarLibrosSocio(socio);
		}

	}

	/**
	 * Metodo para buscar un Socio
	 * 
	 * @param biblioteca05
	 */
	private static void buscarSocio(Biblioteca biblioteca05) {
		boolean error = false;
		Socio socio = null;
		do {
			error = false;
			System.out.println("Da el numero de Socio a Buscar");
			try {
				socio = biblioteca05.buscarSocio(input.nextInt());
				input.nextLine();
			} catch (InputMismatchException e) {
				System.out.println("No es un numero " + e);
				input.nextLine();
				error = true;
			}
		} while (error == true);
		if (socio == null) {
			System.out.println("El Socio no existe");
			intentarNuevamente();
		} else {
			System.out.println(socio);
		}
	}
}
