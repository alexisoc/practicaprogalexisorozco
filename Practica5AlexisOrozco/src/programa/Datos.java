package programa;

import java.time.LocalDate;

import clases.Biblioteca;
import clases.Cuento;
import clases.LibroRecetas;
import clases.Novela;
import clases.Socio;

public class Datos {

	/**
	 * Objetos de Socio
	 * 
	 * @param biblioteca05
	 */
	static void introducirSocios(Biblioteca biblioteca05) {
		biblioteca05.altaSocio("Sam", "Smith Sapiens", "2000-04-27");
		biblioteca05.altaSocio("Ben", "Benedict Benez", "1998-01-04");
		biblioteca05.altaSocio("Nick", "Nicolas Stephen", "2000-11-14");
		biblioteca05.altaSocio("Gary", "Galindo Gerez", "1989-07-17");
		biblioteca05.altaSocio("Charlie", "Charls Michael", "1995-03-22");

	}

	/**
	 * Objetos de los diferentes libros
	 * 
	 * @param biblioteca05
	 */
	static void poblarBiblioteca(Biblioteca biblioteca05) {
		biblioteca05.aniadirCuento("AAA", "El Principito", "Blanco", "Viajes", 40, "Reynal & Hitchcock", 80,
				"1943-04-06");
		biblioteca05.aniadirCuento("AAB", "El Libro de la Selva", "Verde", "Aventuras", 65, "Macmillan Publishers", 15,
				"1894-01-01");
		biblioteca05.aniadirCuento("AAC", "La sirenita", "Azul", "cuento de hadas", 20, "	Ign�c Leopold Kober", 20,
				"1837-04-07");

		biblioteca05.aniadirNovela("BAA", "Los Juegos del Hambre", 374, "Scholastic", 14, "Aventura", "Ciencia ficcion",
				"2008-11-02");
		biblioteca05.aniadirNovela("BAB", "Almendra", 256, "Temas de Hoy", 16, "Amistad", "Drama", "2017-03-31");
		biblioteca05.aniadirNovela("BAC", "IT", 1504, "debols(i)llo", 17, "Terror", "Aventuras", "1986-09-15");

		LibroRecetas libro = biblioteca05.aniadirLibroRecetasPoblar("CAA", "Cocina comida real", 30, "paidos",
				"realfood", 3, "Espa�a", "2020-08-25");
		libro.aniadirReceta("Arroz a la gallega", 130, 5, 12.5);
		libro.aniadirReceta("Tortilla francesa", 45, 3, 7.45);
		libro.aniadirReceta("Tofu terillaqui", 75, 8, 15);
		libro = biblioteca05.aniadirLibroRecetasPoblar("CAB", "Cocina Asiatica", 45, "tartosa", "cenas saludables", 5,
				"India", "1867-05-14");
		libro.aniadirReceta("Pakota de verduras", 45, 5, 12);
		libro.aniadirReceta("Pani Puri", 70, 4, 15);
		libro.aniadirReceta("Kedgeree", 60, 6, 7.5);
		libro = biblioteca05.aniadirLibroRecetasPoblar("CAC", "Cocinillas", 25, "portoroso", "postres", 1, "Turquia",
				"1999-12-20");
		libro.aniadirReceta("Bizcocho Zanahoria", 45, 3, 5.45);
		libro.aniadirReceta("Cupcake de fresa", 70, 7, 7);
		libro.aniadirReceta("Delicias (Turcas)", 120, 2, 13);
	}

	/**
	 * Metodo que a�ade libros que ya estan prestados para poder usar metodos
	 * 
	 * @param biblioteca05
	 */
	public static void librosYaPrestados(Biblioteca biblioteca05) {
		Socio socio;
		biblioteca05.aniadirCuento("AAD", "Pinocho", "Blanco", "Viajes", 60, "Hitchcock", 80, "1943-04-06");
		socio=biblioteca05.buscarSocio(2);
		Cuento cuento=biblioteca05.buscarCuentoCodigo("AAD");
		cuento.setSocio(socio);
		cuento.setFechaPrestamo(LocalDate.parse("2022-02-16"));
		cuento.setFechaDevolucion(LocalDate.parse("2022-03-03"));
		cuento.setEstado(true);
		
		
		biblioteca05.aniadirNovela("BAD", "Divergente", 374, "Scholastic", 14, "Aventura", "Ciencia ficcion",
				"2008-11-02");
		socio=biblioteca05.buscarSocio(1);
		Novela novela=biblioteca05.buscarNovelaCodigo("BAD");
		novela.setSocio(socio);
		novela.setFechaPrestamo(LocalDate.parse("2022-03-05"));
		novela.setFechaDevolucion(LocalDate.parse("2022-03-20"));
		novela.setEstado(true);
	}

	static void dibujoLibro() {
		System.out.println("               *****                        *****                 ");
		System.out.println("               **    ****               ****   **                 ");
		System.out.println("               **       ****        ****       **                 ");
		System.out.println("               **           ********           **                 ");
		System.out.println("               **              **              **                 ");
		System.out.println("               **              **              **                 ");
		System.out.println("               **              **              **                 ");
		System.out.println("               **              **              **                 ");
		System.out.println("               **              **              **                 ");
		System.out.println("               **              **              **                 ");
		System.out.println("               **              **              **                 ");
		System.out.println("               **          **********          **                 ");
		System.out.println("               **     *****         *****      **                 ");
		System.out.println("               ********                **********                 ");
	}
	
	static void textoMenuPrincipal() {
		System.out.println("******************************************************************");
		System.out.println("*                         Menu Principal                         *");
		System.out.println("******************************************************************");
		System.out.println("1.- Datos Novelas");
		System.out.println("2.- Datos Cuentos");
		System.out.println("3.- Datos Libros de Recetas");
		System.out.println("4.- Datos Socio");
		System.out.println("5.- SALIR");
		System.out.println("******************************************************************");
	}

	static void textoMenuNovela() {
		System.out.println("******************************************************************");
		System.out.println("*                           Menu Novela                          *");
		System.out.println("******************************************************************");
		System.out.println("1.- Nueva Novela");
		System.out.println("2.- Lista de las Novelas");
		System.out.println("3.- Buscar Novela");
		System.out.println("4.- Valorar una Novela");
		System.out.println("5.- Coger una Novela");
		System.out.println("6.- Devolver una Novela");
		System.out.println("7.- Dar de Baja una Novela");
		System.out.println("8.- SALIR");
		System.out.println("******************************************************************");
	}

	static void textoMenuCuento() {
		System.out.println("******************************************************************");
		System.out.println("*                           Menu Cuento                          *");
		System.out.println("******************************************************************");
		System.out.println("1.- Nuevo Cuento");
		System.out.println("2.- Lista de los Cuentos");
		System.out.println("3.- Buscar Cuento");
		System.out.println("4.- Valorar un Cuento");
		System.out.println("5.- Coger un Cuento");
		System.out.println("6.- Devolver un Cuento");
		System.out.println("7.- Dar de Baja un Cuento");
		System.out.println("8.- SALIR");
		System.out.println("******************************************************************");
	}

	static void textoMenuLibroRecetas() {
		System.out.println("******************************************************************");
		System.out.println("*                      Menu Libro de Recetas                     *");
		System.out.println("******************************************************************");
		System.out.println("1.- Nueva Libro de Recetas");
		System.out.println("2.- Lista de los Libros de Recetas");
		System.out.println("3.- Buscar Libro de Recetas");
		System.out.println("4.- Valorar un Libro de Recetas");
		System.out.println("5.- Coger un Libro de Recetas");
		System.out.println("6.- Devolver un Libro de Recetas");
		System.out.println("7.- Dar de Baja un Libro de Recetas");
		System.out.println("8.- SALIR");
		System.out.println("******************************************************************");
	}

	static void textoMenuSocio() {
		System.out.println("******************************************************************");
		System.out.println("*                           Menu Socio                           *");
		System.out.println("******************************************************************");
		System.out.println("1.- Nuevo Socio");
		System.out.println("2.- Lista de los Socios");
		System.out.println("3.- Buscar Socio");
		System.out.println("4.- Ver todos los libros que tiene un Socio");
		System.out.println("5.- Dar de Baja un Socio");
		System.out.println("6.- SALIR");
		System.out.println("******************************************************************");
	}

}
