package programa;

import clases.Biblioteca;
/**
 * 
 * @author maoc1
 *@version 1
 */
public class Programa {

	public static void main(String[] args) {

		Biblioteca biblioteca05 = new Biblioteca();

		Datos.introducirSocios(biblioteca05);
		Datos.poblarBiblioteca(biblioteca05);
		Datos.librosYaPrestados(biblioteca05);
		
		MetodosPrograma.menuPrincipal(biblioteca05);

	}
}
