package clases;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public abstract class Libro {
	Scanner input = new Scanner(System.in);
	// Atributos
	/**
	 * Atributos de la clase Libro
	 */
	String codigo;
	String titulo;
	int numPaginas;
	String editorial;
	LocalDate fechaPublicacion;
	boolean estado;
	int valoracion;
	LocalDate fechaPrestamo;
	LocalDate fechaDevolucion;
	Socio socio;

	// Constructores
	/**
	 * Constructor vacio
	 */
	public Libro() {
		this.codigo = "";
		this.titulo = "";
		this.estado = false;
	}

	/**
	 * Constructor usando solo el codigo y el titulo del libro en la biblioteca
	 * 
	 * @param codigo - Codigo que se le da al libro en la biblioteca
	 */
	public Libro(String codigo, String titulo) {
		this.codigo = codigo;
		this.titulo = titulo;
	}

	/**
	 * Constructor usando varios atributos
	 * 
	 * @param codigo
	 * @param titulo
	 * @param numPaginas
	 * @param editorial
	 * @param fechaPublicacion
	 */
	public Libro(String codigo, String titulo, int numPaginas, String editorial) {
		this.codigo = codigo;
		this.titulo = titulo;
		this.numPaginas = numPaginas;
		this.editorial = editorial;
	}

	// Getters y setters
	/**
	 * Getters y Setters de los atributos de la clase Libro
	 * 
	 * @return - Devuelve los valores de los atributos cuando es necesario usarlos
	 */
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getNumPaginas() {
		return numPaginas;
	}

	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public LocalDate getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(LocalDate fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public int getValoracion() {
		return valoracion;
	}

	public void setValoracion(int valoracion) {
		this.valoracion = valoracion;
	}

	public LocalDate getFechaPrestamo() {
		return fechaPrestamo;
	}

	public void setFechaPrestamo(LocalDate fechaPrestamo) {
		this.fechaPrestamo = fechaPrestamo;
	}

	public LocalDate getFechaDevolucion() {
		return fechaDevolucion;
	}

	public void setFechaDevolucion(LocalDate fechaDevoluci�n) {
		this.fechaDevolucion = fechaDevoluci�n;
	}

	public Socio getSocio() {
		return socio;
	}

	public void setSocio(Socio socio) {
		this.socio = socio;
	}

	// toString
	/**
	 * Metodo toString de la clase Libro para mostrar sus datos
	 */
	@Override
	public String toString() {
		return "\nLibro \nCodigo : " + codigo + " \nTitulo : " + titulo + " \nNumero de Paginas : " + numPaginas
				+ " \nEditorial : " + editorial + " \nFecha de Publicacion : " + fechaPublicacion;
	}

	/**
	 * Metodo para mostrar datos, diferente al toString
	 */
	public void mostrarDatos() {
		System.out.println("titulo : " + this.titulo);
		System.out.println("Estado : " + (this.estado ? "Prestado" : "Libre"));
		if (estado == false) {
			System.out.println("El libro est� en la Biblioteca");
		} else {
			System.out.println(this.socio);
			System.out.println("Fecha de prestamo : " + this.fechaPrestamo);
			System.out.println("Fecha de devolucion : " + this.fechaDevolucion);
		}
		System.out.println("Valoracion : " + this.valoracion);
	}
	
	/**
	 * Metodo para aniadir los datos del padre de la herencia por teclado (Libro)
	 * 
	 * @param biblioteca05
	 */
	public void addLibro(Biblioteca biblioteca05) {
		boolean error = true;
		String cod;
		String tit;
		do {
			System.out.println("Introduce el Codigo");
			cod = input.nextLine().toUpperCase();
			if (!biblioteca05.existeLibroCod(cod)) {
				this.codigo = cod;
			} else {
				System.out.println("Este codigo de Libro ya existe");
			}
		} while (biblioteca05.existeLibroCod(cod));

		do {
			System.out.println("Introduce el Titulo");
			tit = input.nextLine();

			if (!biblioteca05.existeLibroTit(tit)) {
				this.titulo = tit;
			} else {
				System.out.println("Este libro ya est� en la biblioteca");
			}
		} while (biblioteca05.existeLibroTit(tit));

		while (error == true) {
			error = false;
			try {
				System.out.println("Introduce el numero de paginas");
				this.numPaginas = input.nextInt();
				error = false;
				input.nextLine();
			} catch (java.util.InputMismatchException e) {
				System.out.println("No es un n�mero \nError " + e);
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Ha ocurrido un error durante el proceso");
				error = true;
			}
		}
		System.out.println("Introduce la Editorial");
		this.editorial = input.nextLine();

		do {
			error = false;

			System.out.println("Introduce la fecha de publicacion con el formato 'AAAA-MM-DD'");
			try {
				this.fechaPublicacion = LocalDate.parse(input.nextLine());

			} catch (DateTimeParseException e) {
				System.out.println("No has introducido una fecha: " + e);
				error = true;

			}

		} while (error == true);

		this.estado = false;
		this.fechaPrestamo = null;
		this.fechaDevolucion = null;
		this.valoracion = 0;
	}

	/**
	 * Metodo para poner las fechas de prestamo y devoluci�n
	 */
	public void fechas() {
		if (socio != null) {
			fechaPrestamo = LocalDate.now();
			fechaDevolucion = LocalDate.now().plusDays(15);
		}
	}

	/**
	 * Metodo que calcula la valoraci�n que tiene un libro si esta es mayor a 10, la
	 * deja en 10 y si es menor a 0 la deja en 0
	 * 
	 * @param prestamo
	 * @return - Devuelve la valoraci�n total de un libro
	 */
	public void valoracionDelPublico() {
		System.out.println("Da una valoracion del 1 al 10 del Libro");
		int puntos = input.nextInt();
		if (puntos > 5 || valoracion < 4) {
			this.valoracion += puntos;
		} else {
			this.valoracion -= puntos;
		}
		if (this.valoracion >= 10) {
			this.valoracion = 10;
		} else if (this.valoracion <= 0) {
			this.valoracion = 0;
		}

	}

	/**
	 * Metodo para coger un libro a la biblioteca
	 * 
	 * @param socio
	 */
	public void cogerLibroPrestado(Socio socio) {
		if (this.isEstado() == false) {
			this.setSocio(socio);
			this.setEstado(true);
			this.fechas();
		} else {
			System.out.println("El libro ya est� prestado");
		}
	}

	/**
	 * Metodo para devolver un libro a la biblioteca
	 * 
	 * @param codigo
	 * @return - devuelve true si el libro estaba prestado y falso si ya estaba en la biblioteca
	 */
	public boolean devolverLibro() {
		boolean devolver=false;
		if (this.isEstado()) {
			this.setSocio(null);
			this.setEstado(false);
			this.setFechaPrestamo(null);
			this.setFechaDevolucion(null);
			devolver=true;
		} 
		return devolver;
	}
	
	/**
	 * Metodo abstracto
	 */
	public abstract void finalidad();
	
}
