package clases;

import java.util.Scanner;

public class Cuento extends Libro {
	Scanner input = new Scanner(System.in);
	// Atributos
	/**
	 * Atributos de la clase cuento
	 */
	String tematica;
	int cantidadIlustraciones;
	String colorPortada;

	// Constructores

	/**
	 * Constructor vacio
	 */
	public Cuento() {
		super();
		this.codigo = "";
		this.titulo = "";
		this.tematica = "";
		this.cantidadIlustraciones = 0;
		this.colorPortada = "";
	}

	/**
	 * Contstructor usando el codigo
	 * 
	 * @param codigo
	 */
	public Cuento(String codigo, String titulo) {
		super(codigo, titulo);
		this.codigo = codigo;
		this.titulo = titulo;
	}

	/**
	 * Constructor completo
	 * 
	 * @param codigo
	 * @param titulo
	 * @param colorPortada
	 * @param tematica
	 * @param numPaginas
	 * @param editorial
	 * @param cantidadIlustraciones
	 * @param fechaPublicacion
	 */
	public Cuento(String codigo, String titulo, String colorPortada, String tematica, int numPaginas, String editorial,
			int cantidadIlustraciones) {
		super(codigo, titulo, numPaginas, editorial);
		this.codigo = codigo;
		this.titulo = titulo;
		this.colorPortada = colorPortada;
		this.tematica = tematica;
		this.numPaginas = numPaginas;
		this.editorial = editorial;
		this.cantidadIlustraciones = cantidadIlustraciones;
	}

	// Getters y setters
	/**
	 * Getters y setters de la clase Cuento
	 * 
	 * @return valor necesario al usar un get*****
	 */
	public String getTematica() {
		return tematica;
	}

	public void setTematica(String tematica) {
		this.tematica = tematica;
	}

	public int getCantidadIlustraciones() {
		return cantidadIlustraciones;
	}

	public void setCantidadIlustraciones(int cantidadIlustraciones) {
		this.cantidadIlustraciones = cantidadIlustraciones;
	}

	public String getColorPortada() {
		return colorPortada;
	}

	public void setColorPortada(String colorPortada) {
		this.colorPortada = colorPortada;
	}

	// toString
	/**
	 * Metodo toString de la clase cuento
	 */
	@Override
	public String toString() {
		return super.toString() + " \nCuento \nTem�tica :" + tematica + " \nCantidad de Ilustraciones : "
				+ cantidadIlustraciones + " \ncolorPortada : " + colorPortada;
	}

	/**
	 * Metodo Abstracto heredado de la clase Libro
	 */
	@Override
	public void finalidad() {
		System.out.println("Realizar peque�as ense�anzas esenciales usando moralejas");
	}

	/**
	 * Metodo para aniadir un cuento por teclado
	 * @param biblioteca05 
	 */
	public void addCuento(Biblioteca biblioteca05) {
		boolean error = true;
		super.addLibro(biblioteca05);
		System.out.println("Introduce el color de la portada");
		this.colorPortada = input.nextLine();
		System.out.println("Introduce la tematica del cuento");
		this.tematica = input.nextLine();
		while (error == true) {
			error = false;
			try {
				System.out.println("Introduce el numero de ilustraciones");
				this.cantidadIlustraciones = input.nextInt();
				input.nextLine();
			} catch (java.util.InputMismatchException e) {
				System.out.println("No es un n�mero \nError " + e);
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Ha ocurrido un error durante el proceso");
				error = true;
			}
		}
	}

	
	/**
	 * Metodo para restar el� numero de paginas si han sido arrancadas
	 * @param paginasArrancadas
	 */
	public void quitarPaginas(int paginasArrancadas) {
		if(paginasArrancadas==0) {
			System.out.println("Enhorabuena, tu peque�o monstruo no ha arrancado ninguna p�gina");
		}else {
			this.numPaginas-=paginasArrancadas;
		}	
	}


}
