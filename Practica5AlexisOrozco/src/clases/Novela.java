package clases;

import java.util.Scanner;

public class Novela extends Libro {

	Scanner input = new Scanner(System.in);
	// Atributos
	int edadRecomendada;
	String genero;
	String subgenero;

	// Constructores
	/**
	 * Constructor vac�o de la clase novela
	 */
	public Novela() {
		super();
	}

	/**
	 * Constructor usando el codigo y el titulo
	 * 
	 * @param codigo
	 * @param titulo
	 */
	public Novela(String codigo, String titulo) {
		super(codigo, titulo);
		this.codigo = codigo;
		this.titulo = titulo;
	}

	/**
	 * Constructor usando varios atributos
	 * 
	 * @param codigo
	 * @param titulo
	 * @param numPaginas
	 * @param editorial
	 * @param fechaPublicacion
	 * @param edadRecomendada
	 * @param genero
	 * @param subgenero
	 */
	public Novela(String codigo, String titulo, int numPaginas, String editorial, int edadRecomendada, String genero,
			String subgenero) {
		super(codigo, titulo, numPaginas, editorial);
		this.codigo = codigo;
		this.titulo = titulo;
		this.numPaginas = numPaginas;
		this.editorial = editorial;
		this.edadRecomendada = edadRecomendada;
		this.genero = genero;
		this.subgenero = subgenero;

	}

	// Getters y setters
	/**
	 * Getters y setters de la clase Novela
	 * 
	 * @return valor necesario al usar un get*****
	 */
	public int getEdadRecomendada() {
		return edadRecomendada;
	}

	public void setEdadRecomendada(int edadRecomendada) {
		this.edadRecomendada = edadRecomendada;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getSubgenero() {
		return subgenero;
	}

	public void setSubgenero(String subgenero) {
		this.subgenero = subgenero;
	}

	/**
	 * Metodo toString de la clase novela
	 */
	@Override
	public String toString() {
		return super.toString() + " \nNovela \nGenero : " + genero + "/" + subgenero + " \nEdad Recomendada : "
				+ edadRecomendada;
	}

	/**
	 * metodo abstracto heredado de la clase Libro
	 */
	@Override
	public void finalidad() {
		System.out.println("Entretener y llenar el vac�o existencial del interior de nuestro coraz�n");

	}

	/**
	 * Metodo para aniadir una novela por teclado
	 * @param biblioteca05 
	 */
	public void addNovela(Biblioteca biblioteca05) {
		boolean error = true;
		super.addLibro(biblioteca05);
		while (error == true) {
			error = false;
			try {
				System.out.println("Introduce la edad recomendada");
				this.edadRecomendada = input.nextInt();
				input.nextLine();
			} catch (java.util.InputMismatchException e) {
				System.out.println("No es un n�mero \nError " + e);
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Ha ocurrido un error durante el proceso" + e);
				error = true;
			}
		}
		System.out.println("Introduce el genero de la novela");
		this.genero = input.nextLine();
		System.out.println("Introduce el subgenero de la novela");
		this.subgenero = input.nextLine();
	}
	
	/**
	 * metodo para aumentar la edad
	 */
	public void aumentarEdad() {
		this.edadRecomendada+=2;
	}

}
