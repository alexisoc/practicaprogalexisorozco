package clases;

import java.util.ArrayList;
import java.util.Scanner;

public class LibroRecetas extends Libro {

	Scanner input = new Scanner(System.in);
	// Atributos
	/**
	 * Atributos clase libroRecetas
	 */
	String tipoRecetas;
	int nivelDificultad;
	String nacionalidadRecetas;
	ArrayList<Receta> listaRecetas;

	// Constructores

	/**
	 * Constructor vac�o
	 */
	public LibroRecetas() {
		super();
		listaRecetas = new ArrayList<Receta>();
	}

	/**
	 * Constructor usando unicamente titulo y codigo
	 * 
	 * @param codigo
	 * @param titulo
	 */
	public LibroRecetas(String codigo, String titulo) {
		super(codigo, titulo);
		this.codigo = codigo;
		this.titulo = titulo;
		listaRecetas = new ArrayList<Receta>();
	}

	/**
	 * Constructor usando parametros
	 * 
	 * @param codigo
	 * @param titulo
	 * @param numPaginas
	 * @param editorial
	 * @param fechaPublicacion
	 */
	public LibroRecetas(String codigo, String titulo, int numPaginas, String editorial, String tipoRecetas,
			int nivelDificultad, String nacionalidadRecetas) {
		super(codigo, titulo, numPaginas, editorial);
		this.codigo = codigo;
		this.titulo = titulo;
		this.numPaginas = numPaginas;
		this.editorial = editorial;
		this.tipoRecetas = tipoRecetas;
		this.nivelDificultad = nivelDificultad;
		this.nacionalidadRecetas = nacionalidadRecetas;
		listaRecetas = new ArrayList<Receta>();
	}

	// getters y setters
	/**
	 * Getters y Setters de los atributos de la clase LibroRecetas
	 * 
	 * @return - Devuelve los valores de los atributos cuando es necesario usarlos
	 */
	public String getTipoRecetas() {
		return tipoRecetas;
	}

	public void setTipoRecetas(String tipoRecetas) {
		this.tipoRecetas = tipoRecetas;
	}

	public int getNivelDificultad() {
		return nivelDificultad;
	}

	public void setNivelDificultad(int nivelDificultad) {
		this.nivelDificultad = nivelDificultad;
	}

	public String getNacionalidadRecetas() {
		return nacionalidadRecetas;
	}

	public void setNacionalidadRecetas(String nacionalidadRecetas) {
		this.nacionalidadRecetas = nacionalidadRecetas;
	}

	// toString
	/**
	 * Metodo to String de la clase libroRecetas
	 */
	@Override
	public String toString() {
		return super.toString() + " \nLibroRecetas \nTipo Recetas : " + tipoRecetas + " \nNivel de dificultad : "
				+ nivelDificultad + " \nPais de origen : " + nacionalidadRecetas;
	}

	/**
	 * Metodo para aniadir las recetas con las que cuenta el libro
	 * 
	 * @param nombre
	 * @param duracion
	 * @param cantidadIngredientes
	 * @param costoAproximado
	 */
	public void aniadirReceta(String nombre, int duracion, int cantidadIngredientes, double costoAproximado) {
		Receta nuevaReceta = new Receta(nombre, duracion, cantidadIngredientes, costoAproximado);
		listaRecetas.add(nuevaReceta);
	}

	/**
	 * Metodo para aniadir receta por teclado
	 */
	public void aniadirReceta() {
		Receta nuevaReceta = new Receta();
		nuevaReceta.addReceta();
		listaRecetas.add(nuevaReceta);
	}

	/**
	 * Metodo para listar las recetas
	 */
	public void listarRecetas() {
		for (Receta receta : listaRecetas) {
			if (receta != null) {
				System.out.println(receta);
			}
		}
	}

	/**
	 * Metodo abstracto heredado de la clase Libro
	 */
	@Override
	public void finalidad() {
		System.out.println("Aprende a cocinar y no te mueras de hambre");

	}

	/**
	 * Metodo para aniadir un libro de recetas por teclado
	 * @param biblioteca05 
	 */
	public void addRecetario(Biblioteca biblioteca05) {
		boolean error = true;
		super.addLibro(biblioteca05);
		System.out.println("Introduce el tipo de recetas que hay en el libro");
		this.tipoRecetas = input.nextLine();
		while (error == true) {
			error = false;
			try {
				System.out.println("Introduce el nivel de dificultad");
				this.nivelDificultad = input.nextInt();
				input.nextLine();
				error = false;
			} catch (java.util.InputMismatchException e) {
				System.out.println("No es un n�mero \nError " + e);
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Ha ocurrido un error durante el proceso");
				error = true;
			}
		}
		System.out.println("Introduce el pais de origen de las recetas");
		this.nacionalidadRecetas = input.nextLine();
	}
}
