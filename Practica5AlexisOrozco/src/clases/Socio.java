package clases;

import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Scanner;

public class Socio {
	Scanner input = new Scanner(System.in);

	// Atributos
	/**
	 * Atributos de la clase Autor
	 */
	private String nombre;
	private String apellidos;
	private int numeroSocio;
	private LocalDate fechaNacimiento;
	private int edad;
	private double multa;
	private int diasRetraso;

	// Constructores
	/**
	 * Constructor vac�o
	 */
	public Socio() {

	}

	/**
	 * Constructor usando atributos
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param numeroSocio
	 */
	public Socio(String nombre, String apellidos) {
		this.nombre = nombre;
		this.apellidos = apellidos;
	}

	// Getters y Setters
	/**
	 * Getters y Setters de los atributos de la clase Autor
	 * 
	 * @return - Devuelve los valores de los atributos cuando es necesario usarlos
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getNumeroSocio() {
		return numeroSocio;
	}

	public void setNumeroSocio(int numeroSocio) {
		this.numeroSocio = numeroSocio;
	}

	public LocalDate getFechaCumpleanios() {
		return fechaNacimiento;
	}

	public void setFechaCumpleanios(LocalDate fechaCumpleanios) {
		this.fechaNacimiento = fechaCumpleanios;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getMulta() {
		return multa;
	}

	public void setMulta(double multa) {
		this.multa = multa;
	}

	public int getDiasRetraso() {
		return diasRetraso;
	}

	public void setDiasRetraso(int diasRetraso) {
		this.diasRetraso = diasRetraso;
	}

	// toString
	/**
	 * Metodo toString para mostrar los datos de Autor
	 */
	@Override
	public String toString() {
		return "\nSocio \nNumero de Socio : " + numeroSocio + " \nNombre : " + nombre + " \nApellidos : " + apellidos
				+ " \nFecha de nacimiento : " + fechaNacimiento + " \nEdad : " + edad;
	}

	/**
	 * Metodo para calcular la edad
	 * 
	 * @param fechaNacimiento
	 * @return
	 */
	public void calcularEdad(String fechaNacimiento) {
		Period anios = Period.between(LocalDate.parse(fechaNacimiento), LocalDate.now());
		this.edad = anios.getYears();
	}

	/**
	 * Metodo que calcula el precio de la multa
	 * 
	 * @return
	 */
	public void obtenerMultaTotalRetraso(Biblioteca biblioteca05, LocalDate fecha) {
		Period dias = Period.between(fecha, LocalDate.now());
		this.diasRetraso = dias.getDays();
		this.multa = ((2.5 + (0.5 * this.diasRetraso)) * 0.79);
	}

	/**
	 * metodo para ver datos diferente a toString
	 */
	public void mostrarDatos() {
		System.out.println("N�mero de Socio : " + this.numeroSocio);
		System.out.println("Nombre : " + this.nombre);
		System.out.println("Apellidos " + this.apellidos);
		System.out.println("Dias de retraso : " + this.diasRetraso);
		System.out.println("Multa : " + this.multa + "�");
	}

	/**
	 * Funciona cuando metemos un socio directamente no por teclado
	 * 
	 * @param fechaNacimiento
	 */
	public void calcularEdad(LocalDate fechaNacimiento) {
		Period anios = Period.between(fechaNacimiento, LocalDate.now());
		this.edad = anios.getYears();
	}

	/**
	 * Metodo para a�adir un socio por teclado
	 * 
	 * @param listaSocios
	 */
	public void addSocio(ArrayList<Socio> listaSocios) {
		boolean error = true;
		this.numeroSocio = (listaSocios.size() + 1);
		System.out.println("Introduce el Nombre del Socio");
		this.nombre = input.nextLine();
		System.out.println("Introduce los apellidos del socio");
		this.apellidos = input.nextLine();
		do {
			error = false;
			System.out.println("Introduce la fecha de nacimiento con el formato 'AAAA-MM-DD'");
			try {
				this.fechaNacimiento = LocalDate.parse(input.nextLine());
			} catch (DateTimeParseException e) {
				System.out.println("No has introducido una fecha: " + e);
				error = true;
			} catch (Exception e) {
				System.out.println("Ha ocurrido un error durante el proceso");
				error = true;
			}
		} while (error == true);
		calcularEdad(fechaNacimiento);
		this.multa = 0;
		this.diasRetraso = 0;
	}

}
