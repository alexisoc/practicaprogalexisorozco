package clases;

import java.util.Scanner;

public class Receta {
	Scanner input = new Scanner(System.in);

	// Atributos
	private String nombre;
	private int duracion;
	private int cantidadIngredientes;
	private double costoAproximado;

	// constructores

	/**
	 * Constructor completo
	 * 
	 * @param nombre
	 * @param duracion
	 * @param cantidadIngredientes
	 * @param costoAproximado
	 */
	public Receta(String nombre, int duracion, int cantidadIngredientes, double costoAproximado) {
		super();
		this.nombre = nombre;
		this.duracion = duracion;
		this.cantidadIngredientes = cantidadIngredientes;
		this.costoAproximado = costoAproximado;
	}

	/**
	 * Constructor vacio
	 */
	public Receta() {

	}

	// getters y setters
	/**
	 * Getters y Setters de los atributos de la clase receta
	 * 
	 * @return - Devuelve los valores de los atributos cuando es necesario usarlos
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getCantidadIngredientes() {
		return cantidadIngredientes;
	}

	public void setCantidadIngredientes(int cantidadIngredientes) {
		this.cantidadIngredientes = cantidadIngredientes;
	}

	public double getCostoAproximado() {
		return costoAproximado;
	}

	public void setCostoAproximado(double costoAproximado) {
		this.costoAproximado = costoAproximado;
	}

	// toString
	/**
	 * toString de la clase receta
	 */
	@Override
	public String toString() {
		return "************************* \nReceta \nNombre : " + nombre + " \nTiempo de preparacion : " + duracion
				+ " min" + " \nCantidad de ingredientes necesarios : " + cantidadIngredientes + " \nCosto aproximado : "
				+ costoAproximado + "\n*************************";
	}

	/**
	 * Metodo para aniadir un Libro de recetas por teclado
	 */
	public void addReceta() {
		boolean error = true;
		System.out.println("Introduce el Nombre de la Receta");
		this.nombre = input.nextLine();
		while (error == true) {
			error = false;
			try {
				System.out.println("Introduce el tiempo de preparacion de la receta");
				this.duracion = input.nextInt();
				System.out.println("Introduce la cantidad de ingredientes necesarios");
				this.cantidadIngredientes = input.nextInt();
				System.out.println("Introduce el costo aproximado de la receta");
				this.costoAproximado = input.nextDouble();
				error = false;
			} catch (java.util.InputMismatchException e) {
				System.out.println("No es un n�mero \nError " + e);
				input.nextLine();
				error = true;
			} catch (Exception e) {
				System.out.println("Ha ocurrido un error durante el proceso");
				error = true;
			}
		}
	}
}
