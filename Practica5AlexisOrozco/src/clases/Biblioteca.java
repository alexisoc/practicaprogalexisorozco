package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;

public class Biblioteca {

	// ArrayList
	/**
	 * Declaro los ArrayList de las diferentes partes que conforman el total d ela
	 * biblioteca
	 */
	private ArrayList<Socio> listaSocios;
	private ArrayList<Cuento> listaCuentos;
	private ArrayList<Novela> listaNovelas;
	private ArrayList<LibroRecetas> listaRecetarios;

	// constructor
	/**
	 * Constructor de la clase Biblioteca
	 */
	public Biblioteca() {
		listaSocios = new ArrayList<Socio>();
		listaCuentos = new ArrayList<Cuento>();
		listaNovelas = new ArrayList<Novela>();
		listaRecetarios = new ArrayList<LibroRecetas>();
	}

	// METODOS SOCIO
	/**
	 * Metodo dar de alta un Socio
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param numeroSocio
	 * @param fechaNacimiento
	 */
	public void altaSocio(String nombre, String apellidos, String fechaNacimiento) {
		Socio nuevoSocio = new Socio(nombre, apellidos);
		nuevoSocio.setNumeroSocio(listaSocios.size() + 1);
		nuevoSocio.setFechaCumpleanios(LocalDate.parse(fechaNacimiento));
		nuevoSocio.calcularEdad(fechaNacimiento);
		nuevoSocio.setDiasRetraso(0);
		nuevoSocio.setMulta(0);
		listaSocios.add(nuevoSocio);
	}

	/**
	 * Metodo que llama al metodo para introducir Socios por teclado
	 */
	public void altaSocio() {
		Socio nuevoSocio = new Socio();
		nuevoSocio.addSocio(listaSocios);
		listaSocios.add(nuevoSocio);
	}

	/**
	 * Metodo para realizar una lista de los Socios
	 */
	public void listarSocios() {
		for (Socio socio : listaSocios) {
			if (socio != null) {
				System.out.println(socio);
			}
		}
	}

	/**
	 * Metodo mostrar datos de Socio
	 */
	public void mostrarDatosSocio() {
		for (Socio socio : listaSocios) {
			System.out.println("Nombre" + socio.getNombre());
			System.out.println("Apellidos : " + socio.getApellidos());
			System.out.println("Multa : " + socio.getMulta());
			System.out.println("Dias de retraso : " + socio.getDiasRetraso());
			System.out.println(" ");
		}
	}

	/**
	 * Metodo para buscar un Socio por su numero de Socio
	 * 
	 * @param numeroSocio
	 * @return - Devuelve el socio que hemos buscado
	 */
	public Socio buscarSocio(int numeroSocio) {
		for (Socio socio : listaSocios) {
			if (socio != null && socio.getNumeroSocio() == numeroSocio) {
				return socio;
			}
		}
		return null;
	}

	/**
	 * Metodo para dar de baja un Socio
	 * 
	 * @param numeroSocio
	 */
	public void darBajaSocio(int numeroSocio) {
		Iterator<Socio> iteradorSocios = listaSocios.iterator();
		while (iteradorSocios.hasNext()) {
			Socio socio = iteradorSocios.next();
			if (socio.getNumeroSocio() == numeroSocio) {
				iteradorSocios.remove();
			}
		}
	}

	// METODOS CUENTO

	/**
	 * Metodo para introducir un nuevo libro
	 * 
	 * @param codigo
	 * @param titulo
	 * @param colorPortada
	 * @param tematica
	 * @param numPaginas
	 * @param editorial
	 * @param cantidadIlustraciones
	 * @param fechaPublicacion
	 */
	public void aniadirCuento(String codigo, String titulo, String colorPortada, String tematica, int numPaginas,
			String editorial, int cantidadIlustraciones, String fechaPublicacion) {
		Cuento nuevoCuento = new Cuento(codigo, titulo, colorPortada, tematica, numPaginas, editorial,
				cantidadIlustraciones);
		nuevoCuento.setFechaPublicacion(LocalDate.parse(fechaPublicacion));
		nuevoCuento.setEstado(false);
		nuevoCuento.setFechaPrestamo(null);
		nuevoCuento.setFechaDevolucion(null);
		nuevoCuento.setValoracion(0);
		listaCuentos.add(nuevoCuento);
	}

	/**
	 * Metodo que llama al metodo para introducir cuentos por teclado
	 * 
	 * @param biblioteca05
	 */
	public void aniadirCuento(Biblioteca biblioteca05) {
		Cuento nuevoCuento = new Cuento();
		nuevoCuento.addCuento(biblioteca05);
		listaCuentos.add(nuevoCuento);
	}

	/**
	 * Metodo para listar los cuentos
	 */
	public void listarCuentos() {
		for (Cuento cuento : listaCuentos) {
			if (cuento != null) {
				System.out.println(cuento);
				cuento.finalidad();
			}
		}
	}

	/**
	 * Metodo para buscar un cuento por el titulo
	 * 
	 * @param titulo
	 * @return - devuelve el cuento que hemos buscado
	 */
	public Cuento buscarCuentoTitulo(String titulo) {
		for (Cuento cuento : listaCuentos) {
			if (cuento != null && cuento.getTitulo().equalsIgnoreCase(titulo)) {
				return cuento;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar un cuento por el codigo
	 * 
	 * @param titulo
	 * @return - devuelve el cuento que hemos buscado
	 */
	public Cuento buscarCuentoCodigo(String codigo) {
		for (Cuento cuento : listaCuentos) {
			if (cuento != null && cuento.getCodigo().equals(codigo)) {
				return cuento;
			}
		}
		return null;
	}

	/**
	 * Metodo para eliminar un Cuento
	 * 
	 * @param codigo
	 */
	public void cuentoDescatalogado(String codigo) {
		Iterator<Cuento> iteradorCuentos = listaCuentos.iterator();
		while (iteradorCuentos.hasNext()) {
			Cuento cuento = iteradorCuentos.next();
			if (cuento.getCodigo().equals(codigo)) {
				iteradorCuentos.remove();
			}
		}
	}

	// METODOS NOVELAS

	/**
	 * Metodo para aniadir una novela
	 * 
	 * @param codigo
	 * @param titulo
	 * @param numPaginas
	 * @param editorial
	 * @param edadRecomendada
	 * @param genero
	 * @param subgenero
	 * @param fechaPublicacion
	 */
	public void aniadirNovela(String codigo, String titulo, int numPaginas, String editorial, int edadRecomendada,
			String genero, String subgenero, String fechaPublicacion) {
		Novela nuevaNovela = new Novela(codigo, titulo, numPaginas, editorial, edadRecomendada, genero, subgenero);
		nuevaNovela.setFechaPublicacion(LocalDate.parse(fechaPublicacion));
		nuevaNovela.setEstado(false);
		nuevaNovela.setFechaPrestamo(null);
		nuevaNovela.setFechaDevolucion(null);
		nuevaNovela.setValoracion(0);
		listaNovelas.add(nuevaNovela);
	}

	/**
	 * Metodo que llama al metodo para introducir Novelas por teclado
	 * 
	 * @param biblioteca05
	 */
	public void aniadirNovela(Biblioteca biblioteca05) {
		Novela nuevaNovela = new Novela();
		nuevaNovela.addNovela(biblioteca05);
		listaNovelas.add(nuevaNovela);
	}

	/**
	 * Metodo para listar novelas
	 */
	public void listarNovelas() {
		for (Novela novela : listaNovelas) {
			if (novela != null) {
				System.out.println(novela);
				System.out.println("Finalidad : ");
				novela.finalidad();
			}
		}
	}

	/**
	 * Metodo paara buscar novelas por titulo
	 * 
	 * @param titulo
	 * @return - devuelve la novela q buscas
	 */
	public Novela buscarNovelaTitulo(String titulo) {
		for (Novela novela : listaNovelas) {
			if (novela != null && novela.getTitulo().equalsIgnoreCase(titulo)) {
				return novela;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar novelas por Codigo
	 * 
	 * @param titulo
	 * @return - devuelve la novela q buscas
	 */
	public Novela buscarNovelaCodigo(String codigo) {
		for (Novela novela : listaNovelas) {
			if (novela != null && novela.getCodigo().equals(codigo)) {
				return novela;
			}
		}
		return null;
	}

	/**
	 * Metodo para eliminar una novela
	 * 
	 * @param codigo
	 */
	public void novelaDescatalogada(String codigo) {
		Iterator<Novela> iteradorNovelas = listaNovelas.iterator();
		while (iteradorNovelas.hasNext()) {
			Novela novela = iteradorNovelas.next();
			if (novela.getCodigo().equals(codigo)) {
				iteradorNovelas.remove();
			}
		}
	}

	// METODOS LIBROS DE RECETAS

	/**
	 * Metodo para introducir un nuevo libro de recetas
	 * 
	 * @param codigo
	 * @param titulo
	 * @param numPaginas
	 * @param editorial
	 * @param tipoRecetas
	 * @param nivelDificultad
	 * @param nacionalidadRecetas
	 * @param fechaPublicacion
	 */
	public void aniadirLibroRecetas(String codigo, String titulo, int numPaginas, String editorial, String tipoRecetas,
			int nivelDificultad, String nacionalidadRecetas, String fechaPublicacion) {
		LibroRecetas nuevoRecetario = new LibroRecetas(codigo, titulo, numPaginas, editorial, tipoRecetas,
				nivelDificultad, nacionalidadRecetas);
		nuevoRecetario.setFechaPublicacion(LocalDate.parse(fechaPublicacion));
		nuevoRecetario.setEstado(false);
		nuevoRecetario.setFechaPrestamo(null);
		nuevoRecetario.setFechaDevolucion(null);
		nuevoRecetario.setValoracion(0);
		listaRecetarios.add(nuevoRecetario);
	}

	/**
	 * Metodo que llama al metodo para introducir Libros de recetas por teclado y lo
	 * devuelve para poder trabajar con el
	 * 
	 * @param biblioteca05
	 * 
	 * @return - devuelve el objeto que creas
	 */
	public LibroRecetas aniadirLibroRecetas(Biblioteca biblioteca05) {
		LibroRecetas nuevoRecetario = new LibroRecetas();
		nuevoRecetario.addRecetario(biblioteca05);
		listaRecetarios.add(nuevoRecetario);
		return nuevoRecetario;
	}

	/**
	 * Metodo para crear un libro de recetas y que nos permita poblarlo
	 * 
	 * @param codigo
	 * @param titulo
	 * @param numPaginas
	 * @param editorial
	 * @param tipoRecetas
	 * @param nivelDificultad
	 * @param nacionalidadRecetas
	 * @param fechaPublicacion
	 * @return - Devuelve el libro de recetas que creamos para poder usarlo
	 */
	public LibroRecetas aniadirLibroRecetasPoblar(String codigo, String titulo, int numPaginas, String editorial,
			String tipoRecetas, int nivelDificultad, String nacionalidadRecetas, String fechaPublicacion) {
		LibroRecetas nuevoRecetario = new LibroRecetas(codigo, titulo, numPaginas, editorial, tipoRecetas,
				nivelDificultad, nacionalidadRecetas);
		nuevoRecetario.setFechaPublicacion(LocalDate.parse(fechaPublicacion));
		nuevoRecetario.setEstado(false);
		nuevoRecetario.setFechaPrestamo(null);
		nuevoRecetario.setFechaDevolucion(null);
		nuevoRecetario.setValoracion(0);
		listaRecetarios.add(nuevoRecetario);
		return nuevoRecetario;
	}

	/**
	 * Metodo para listar libros de recetas
	 */
	public void listarLibroRecetas() {
		for (LibroRecetas recetario : listaRecetarios) {
			if (recetario != null) {
				System.out.println(recetario);
				recetario.finalidad();
			}
		}
	}

	public void listarLibroRecetas2() {
		for (LibroRecetas recetario : listaRecetarios) {
			if (recetario != null) {
				System.out.println(recetario);
				recetario.listarRecetas();
			}
		}
	}

	/**
	 * Metodo para buscar un libro de recetas por titulo
	 * 
	 * @param titulo
	 * @return - devuelve el libro de recetas que buscas
	 */
	public LibroRecetas buscarRecetarioTitulo(String titulo) {
		for (LibroRecetas recetario : listaRecetarios) {
			if (recetario != null && recetario.getTitulo().equalsIgnoreCase(titulo)) {
				return recetario;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar un libro de recetas por codigo
	 * 
	 * @param titulo
	 * @return - devuelve el libro de recetas que buscas
	 */
	public LibroRecetas buscarRecetarioCodigo(String codigo) {
		for (LibroRecetas recetario : listaRecetarios) {
			if (recetario != null && recetario.getCodigo().equals(codigo)) {
				return recetario;
			}
		}
		return null;
	}

	/**
	 * Metodo para eliminar un libro de recetas
	 * 
	 * @param codigo
	 */
	public void recetarioDescatalogado(String codigo) {
		Iterator<LibroRecetas> iteradorRecetarios = listaRecetarios.iterator();
		while (iteradorRecetarios.hasNext()) {
			LibroRecetas recetario = iteradorRecetarios.next();
			if (recetario.getCodigo().equals(codigo)) {
				iteradorRecetarios.remove();
			}
		}
	}

	// MAS METODOS

	/**
	 * MEtodo para comprobar si un codigo ya existe
	 * 
	 * @param cod
	 * @return -devuelve true o false en caso de q exista o no
	 */
	public boolean existeLibroCod(String cod) {
		boolean existe = false;

		for (Cuento cuento : listaCuentos) {
			if (cuento.getCodigo().equals(cod)) {
				existe = true;
			}
		}
		for (Novela novela : listaNovelas) {
			if (novela.getCodigo().equals(cod)) {
				existe = true;
			}
		}
		for (LibroRecetas recetario : listaRecetarios) {
			if (recetario.getCodigo().equals(cod)) {
				existe = true;
			}
		}
		return existe;
	}

	/**
	 * MEtodo para comprobar si un titulo ya existe
	 * 
	 * @param tit
	 * @return -devuelve true o false en caso de q exista o no
	 */
	public boolean existeLibroTit(String tit) {
		boolean existe = false;

		for (Cuento cuento : listaCuentos) {
			if (cuento.getTitulo().equalsIgnoreCase(tit)) {
				existe = true;
			}
		}
		for (Novela novela : listaNovelas) {
			if (novela.getTitulo().equalsIgnoreCase(tit)) {
				existe = true;
			}
		}
		for (LibroRecetas recetario : listaRecetarios) {
			if (recetario.getTitulo().equalsIgnoreCase(tit)) {
				existe = true;
			}
		}
		return existe;
	}

	/**
	 * metodo para listar todos los libros que tiene en prestamo un socio
	 * 
	 * @param socio
	 */
	public void listarLibrosSocio(Socio socio) {
		boolean existe = false;
		for (Cuento cuento : listaCuentos) {
			if (cuento.getSocio() == socio) {
				System.out.println(cuento);
				existe = true;
			}
		}
		for (Novela novela : listaNovelas) {
			if (novela.getSocio() == socio) {
				System.out.println(novela);
				existe = true;
			}
		}
		for (LibroRecetas recetario : listaRecetarios) {
			if (recetario.getSocio() == socio) {
				System.out.println(recetario);
				existe = true;
			}
		}
		if (existe == false) {
			System.out.println("El socio no tiene libros");
		}

	}

}
