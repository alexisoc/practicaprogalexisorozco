package clases;

public class Novela extends Libro {

	// Atributos
	/**
	 * Atributos clase Novela
	 */
	private static int contadorNovelas;

	private String genero;
	private int edadRecomendada;

	// Constructores

	/**
	 * Constructor usando unicamente el genero literario
	 * 
	 * @param genero
	 */
	// constructor solo por el genero literario
	public Novela(String genero) {
		this.genero = genero;
		contadorNovelas++;
	}

	/**
	 * Constructor completo clase Novela
	 * 
	 * @param titulo
	 * @param editorial
	 * @param nombreAutor
	 * @param apellidoAutor
	 * @param numPaginas
	 * @param edicion
	 * @param codLibro
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param fechaEdicion
	 * @param tapaDura
	 * @param sobrecubierta
	 * @param lgbtq
	 * @param prestado
	 * @param genero
	 * @param edadRecomendada
	 */

	public Novela(String titulo, String editorial, String nombreAutor, String apellidoAutor, int numPaginas,
			int edicion, int codLibro, double valoracionMedia, String fechaPublicacion, String fechaEdicion,
			boolean tapaDura, boolean sobrecubierta, boolean lgbtq, boolean prestado, String genero,
			int edadRecomendada) {
		super(titulo, editorial, nombreAutor, apellidoAutor, numPaginas, edicion, codLibro, valoracionMedia,
				fechaPublicacion, fechaEdicion, tapaDura, sobrecubierta, lgbtq, prestado);
		this.genero = genero;
		this.edadRecomendada = edadRecomendada;
		contadorNovelas++;
	}

	// Getters y setters
	/**
	 * Setters y getters clase Novela
	 * 
	 * @return
	 */
	public static int getContadorNovelas() {
		return contadorNovelas;
	}

	public static void setContadorNovelas(int contadorNovelas) {
		Novela.contadorNovelas = contadorNovelas;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getEdadRecomendada() {
		return edadRecomendada;
	}

	public void setEdadRecomendada(int edadRecomendada) {
		this.edadRecomendada = edadRecomendada;
	}

	// toString
	/**
	 * toString clase Novela
	 */
	@Override
	public String toString() {
		return super.toString() + " \nGenero : " + genero + " \nEdad Recomendada : " + edadRecomendada + "\n";
	}

}
