package clases;

import java.time.LocalDate;

public class Cd {

	// Atributos
	/**
	 * Atributos clase Cd
	 */
	private String titulo;
	private String artista;
	private int duracion;
	private int codCd;
	private double valoracionMedia;
	private LocalDate fechaPublicacion;
	private boolean prestado;

	// constructores

	// constructor solo con el codigo
	/**
	 * Constructor usando unicamente el codigo de Dvd
	 * 
	 * @param codDvd
	 */
	public Cd(int codCd) {
		this.codCd = codCd;
	}

	// Constructor completo Dvd
	/**
	 * Constructor completo Dvd
	 * 
	 * @param titulo
	 * @param artista
	 * @param duracion
	 * @param codDvd           codigo por el que se introduce el Cd al programa
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param prestado
	 */
	public Cd(String titulo, String artista, int duracion, int codCd, double valoracionMedia,
			LocalDate fechaPublicacion, boolean prestado) {
		super();
		this.titulo = titulo;
		this.duracion = duracion;
		this.codCd = codCd;
		this.valoracionMedia = valoracionMedia;
		this.fechaPublicacion = fechaPublicacion;
		this.prestado = prestado;
	}

	// Getters y Setters
	/**
	 * Getters y setters clase Cd
	 * 
	 * @return
	 */
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getArtista() {
		return artista;
	}

	public void setArtista(String artista) {
		this.artista = artista;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getCodCd() {
		return codCd;
	}

	public void setCodCd(int codCd) {
		this.codCd = codCd;
	}

	public double getValoracionMedia() {
		return valoracionMedia;
	}

	public void setValoracionMedia(double valoracionMedia) {
		this.valoracionMedia = valoracionMedia;
	}

	public LocalDate getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(LocalDate fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public boolean isPrestado() {
		return prestado;
	}

	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}

	// toString
	/**
	 * toString clase Cd
	 */
	@Override
	public String toString() {
		return "CD \nTitulo : " + titulo + " \nArtista : " + artista + " \nDuración : " + duracion + " minutos"
				+ " \nCodigo de Cd : " + codCd + " \nValoración Media : " + valoracionMedia
				+ " \nFecha de Publicacion : " + fechaPublicacion + "\nEstado de prestamo : " + prestado + "\n";
	}

}
