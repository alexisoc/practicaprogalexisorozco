package clases;

import java.time.LocalDate;

public class Dvd {

	// Atributos
	/**
	 * Atributos de la clase Dvd
	 */
	private String titulo;
	private int duracion;
	private int codDvd;
	private double valoracionMedia;
	private LocalDate fechaPublicacion;
	private boolean lgbtq;
	private boolean prestado;

	// constructores

	// constructor solo con el codigo
	/**
	 * Constructor usando unicamente el codigo de Dvd
	 * 
	 * @param codDvd
	 */
	public Dvd(int codDvd) {
		this.codDvd = codDvd;
	}

	// Constructor completo Dvd
	/**
	 * Constructor completo Dvd
	 * 
	 * @param titulo
	 * @param duracion
	 * @param codDvd           codigo por el que se introduce el dvd al programa
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param lgbtq
	 * @param prestado
	 */
	public Dvd(String titulo, int duracion, int codDvd, double valoracionMedia, LocalDate fechaPublicacion,
			boolean lgbtq, boolean prestado) {
		this.titulo = titulo;
		this.duracion = duracion;
		this.codDvd = codDvd;
		this.valoracionMedia = valoracionMedia;
		this.fechaPublicacion = fechaPublicacion;
		this.lgbtq = lgbtq;
		this.setPrestado(prestado);
	}

	// Getters y Setters
	/**
	 * Setters y getters clase dvd
	 * 
	 * @return
	 */
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getCodDvd() {
		return codDvd;
	}

	public void setCodDvd(int codDvd) {
		this.codDvd = codDvd;
	}

	public double getValoracionMedia() {
		return valoracionMedia;
	}

	public void setValoracionMedia(double valoracionMedia) {
		this.valoracionMedia = valoracionMedia;
	}

	public LocalDate getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(LocalDate fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public boolean isLgbtq() {
		return lgbtq;
	}

	public void setLgbtq(boolean lgbtq) {
		this.lgbtq = lgbtq;
	}

	public boolean isPrestado() {
		return prestado;
	}

	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}

	// toString
	/**
	 * toString clase Dvd
	 */
	@Override
	public String toString() {
		return "Disco DVD \nTitulo : " + titulo + " \nDuración : " + duracion + " minutos" + " \nCodigo de Dvd : "
				+ codDvd + " \nValoración Media : " + valoracionMedia + " \nFecha de Publicacion : " + fechaPublicacion
				+ " \nlgbtq : " + lgbtq + "\nEstado de prestamo : " + prestado + "\n";
	}

}
