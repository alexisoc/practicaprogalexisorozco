package clases;

import java.time.LocalDate;

public class Socio {

	// Atributos
	/**
	 * Atributos clase Socio
	 */
	private String nombre;
	private String apellidos;
	private int numSocio;
	private LocalDate fechaNacimiento;
	private int edad;
	private double multaExtravioDanio;
	private double multaRetrasoDevolucion;
	private int diasRetraso;

	// Constructores
	/**
	 * Constructor vacio de Socio
	 */
	public Socio() {

	}

	/**
	 * Constructor unicamente con Numero de socio
	 * 
	 * @param numSocio Numero de Registro de Socio
	 */
	public Socio(int numSocio) {
		this.numSocio = numSocio;
	}

	/**
	 * Constructor con dos datos
	 * 
	 * @param nombre
	 * @param numSocio
	 */
	public Socio(String nombre, int numSocio) {
		this.nombre = nombre;
		this.numSocio = numSocio;
	}

	/**
	 * Constructor completo
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param numSocio               numero por el que se introducen lso Socios al
	 *                               programa
	 * @param fechaNacimiento
	 * @param edad
	 * @param multaExtravioDanio
	 * @param multaRetrasoDevolucion
	 * @param diasRetraso
	 */
	public Socio(String nombre, String apellidos, int numSocio, String fechaNacimiento, int edad,
			double multaExtravioDanio, double multaRetrasoDevolucion, int diasRetraso) {
		super();
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.numSocio = numSocio;
		this.fechaNacimiento = LocalDate.parse(fechaNacimiento);
		this.edad = edad;
		this.multaExtravioDanio = multaExtravioDanio;
		this.multaRetrasoDevolucion = multaRetrasoDevolucion;
		this.diasRetraso = diasRetraso;
	}

	// Setters y Getters
	/**
	 * Getters y Setters clase Socio
	 * 
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getNumSocio() {
		return numSocio;
	}

	public void setNumSocio(int numSocio) {
		this.numSocio = numSocio;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public double getMultaExtravioDanio() {
		return multaExtravioDanio;
	}

	public void setMultaExtravioDanio(double multaExtravioDanio) {
		this.multaExtravioDanio = multaExtravioDanio;
	}

	public double getMultaRetrasoDevolucion() {
		return multaRetrasoDevolucion;
	}

	public void setMultaRetrasoDevolucion(double multaRetrasoDevolucion) {
		this.multaRetrasoDevolucion = multaRetrasoDevolucion;
	}

	public int getDiasRetraso() {
		return diasRetraso;
	}

	public void setDiasRetraso(int diasRetraso) {
		this.diasRetraso = diasRetraso;
	}

	// Metodos
	/**
	 * Metodo para calcular el precio final por da�o o perdida
	 * 
	 * @return
	 */
	public String obtenerMultaTotalDanio() {
		return ("Multa total por el da�o o Perdida : " + (this.multaExtravioDanio + (this.multaExtravioDanio * 0.21))+"�");
	}

	/**
	 * Metodo que calcula el precio final por retraso
	 * 
	 * @return
	 */
	public String obtenerMultaTotalRetraso() {
		return ("Multa total por el retraso en la devoluci�n : "
				+ (this.multaRetrasoDevolucion + (0.5 * this.diasRetraso))+"�" );
	}

	// toString
	/**
	 * toString clase Socio
	 */
	@Override
	public String toString() {
		return "\nSocio \nNombre : " + nombre + " \nApellidos : " + apellidos + " \nNumero de Socio : " + numSocio
				+ " \nFecha de nacimiento : " + fechaNacimiento + " \nEdad : " + edad
				+ " \nMulta por Extravio o Da�o : " + multaExtravioDanio +"�"+  " \nMulta por Retraso en la Devolucion : "
				+ multaRetrasoDevolucion +"�"+ " \nDias de Retraso en la Devoluci�n : " + diasRetraso;
	}

}
