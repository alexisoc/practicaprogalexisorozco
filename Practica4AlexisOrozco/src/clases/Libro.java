package clases;

import java.time.LocalDate;

public class Libro {

	static private int contadorLibros;

	// Atributos
	/**
	 * Atributos de la clase Libro
	 */
	private String titulo;
	private String editorial;
	private String nombreAutor;
	private String apellidoAutor;
	private int numPaginas;
	private int edicion;
	private int codLibro;
	private double valoracionMedia;
	private LocalDate fechaPublicacion;
	private LocalDate fechaEdicion;
	private boolean tapaDura;
	private boolean sobrecubierta;
	private boolean lgbtq;
	private boolean prestado;

	// Constructores

	/**
	 * Constructor Vac�o
	 */
	public Libro() {
		contadorLibros++;
	}

	/**
	 * Constructor Que usa unicamente el codigo que se le da al Libro
	 * 
	 * @param codLibro C�digo de registro de Libro
	 */
	public Libro(int codLibro) {
		this.codLibro = codLibro;
		contadorLibros++;
	}

	/**
	 * Constructor con todos los datos para libros que su autor firme con su nombre
	 * y apellido
	 * 
	 * @param titulo
	 * @param editorial
	 * @param nombreAutor
	 * @param apellidoAutor
	 * @param numPaginas
	 * @param numCapitulos
	 * @param edicion
	 * @param codLibro
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param fechaEdicion
	 * @param tapaDura
	 * @param sobrecubierta
	 * @param lgbtq
	 * @param prestado
	 */
	public Libro(String titulo, String editorial, String nombreAutor, String apellidoAutor, int numPaginas, int edicion,
			int codLibro, double valoracionMedia, String fechaPublicacion, String fechaEdicion, boolean tapaDura,
			boolean sobrecubierta, boolean lgbtq, boolean prestado) {
		this.titulo = titulo;
		this.editorial = editorial;
		this.nombreAutor = nombreAutor;
		this.apellidoAutor = apellidoAutor;
		this.numPaginas = numPaginas;
		this.edicion = edicion;
		this.codLibro = codLibro;
		this.valoracionMedia = valoracionMedia;
		this.fechaPublicacion = LocalDate.parse(fechaPublicacion);
		this.fechaEdicion = LocalDate.parse(fechaEdicion);
		this.tapaDura = tapaDura;
		this.sobrecubierta = sobrecubierta;
		this.lgbtq = lgbtq;
		this.prestado = prestado;

		contadorLibros++;
	}

	// Setter y Getter
	/**
	 * Setters y getters clase Libro
	 */
	public static int getContadorLibros() {
		return contadorLibros;
	}

	public static void setContadorLibros(int contadorLibros) {
		Libro.contadorLibros = contadorLibros;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}

	public String getNombreAutor() {
		return nombreAutor;
	}

	public void setNombreAutor(String nombreAutor) {
		this.nombreAutor = nombreAutor;
	}

	public String getApellidoAutor() {
		return apellidoAutor;
	}

	public void setApellidoAutor(String apellidoAutor) {
		this.apellidoAutor = apellidoAutor;
	}

	public int getNumPaginas() {
		return numPaginas;
	}

	public void setNumPaginas(int numPaginas) {
		this.numPaginas = numPaginas;
	}

	public int getEdicion() {
		return edicion;
	}

	public void setEdicion(int edicion) {
		this.edicion = edicion;
	}

	public int getCodLibro() {
		return codLibro;
	}

	public void setCodLibro(int codLibro) {
		this.codLibro = codLibro;
	}

	public double getValoracionMedia() {
		return valoracionMedia;
	}

	public void setValoracionMedia(double valoracionMedia) {
		this.valoracionMedia = valoracionMedia;
	}

	public boolean isTapaDura() {
		return tapaDura;
	}

	public void setTapaDura(boolean tapaDura) {
		this.tapaDura = tapaDura;
	}

	public boolean isSobrecubierta() {
		return sobrecubierta;
	}

	public void setSobrecubierta(boolean sobrecubierta) {
		this.sobrecubierta = sobrecubierta;
	}

	public boolean isLgbtq() {
		return lgbtq;
	}

	public void setLgbtq(boolean lgbtq) {
		this.lgbtq = lgbtq;
	}

	public boolean isPrestado() {
		return prestado;
	}

	public void setPrestado(boolean prestado) {
		this.prestado = prestado;
	}

	public LocalDate getFechaPublicacion() {
		return fechaPublicacion;
	}

	public void setFechaPublicacion(LocalDate fechaPublicacion) {
		this.fechaPublicacion = fechaPublicacion;
	}

	public LocalDate getFechaEdicion() {
		return fechaEdicion;
	}

	public void setFechaEdicion(LocalDate fechaEdicion) {
		this.fechaEdicion = fechaEdicion;
	}

	// toString
	/**
	 * toString clase Libro
	 */
	@Override
	public String toString() {
		return "Titulo : " + titulo + " \nEditorial : " + editorial + " \nNombre del Autor : " + nombreAutor
				+ " \nApellido del Autor : " + apellidoAutor + " \nnumPaginas : " + numPaginas + " \nEdicion : "
				+ edicion + " \nCodigo de Libro : " + codLibro + " \nValoraci�n Media : " + valoracionMedia
				+ " \nFecha de Publicaci�n : " + fechaPublicacion + " \nFecha de Edici�n : " + fechaEdicion
				+ " \nTapa Dura : " + tapaDura + " \nSobrecubierta : " + sobrecubierta + " \nlgbtq : " + lgbtq
				+ "\nEstado de prestamo : " + prestado + "\n";
	}

}
