package clases;

import java.time.LocalDate;

public class Biblioteca {

	// Atributos
	private Libro[] libros;
	private Novela[] novelas;
	private Dvd[] dvds;
	private Cd[] cds;
	private Socio[] socios;

	// Constructor
	/**
	 * Constructor completo de la clase biblioteca
	 * 
	 * @param maxLibros  Array de libros de la cantidad deseada
	 * @param maxNovelas Array de Novelas de la cantidad deseada
	 * @param maxDvds    Array de Dvd's de la cantidad deseada
	 * @param maxCds     Array de Cd's de la cantidad deseada
	 * @param maxSocios  Array de Socios de la cantidad deseada
	 */
	public Biblioteca(int maxLibros, int maxNovelas, int maxDvds, int maxCds, int maxSocios) {
		this.libros = new Libro[maxLibros];
		this.novelas = new Novela[maxNovelas];
		this.socios = new Socio[maxSocios];
		this.dvds = new Dvd[maxDvds];
		this.cds = new Cd[maxCds];
	}

	// ************************************************************************************************
	// METODOS LIBRO
	// ************************************************************************************************
	// A�adir Libro
	/**
	 * Metodo para A�adir Libros, partiendo uhniocamente por el codigo de
	 * inscripcion
	 * 
	 * @param titulo
	 * @param editorial
	 * @param nombreAutor
	 * @param apellidoAutor
	 * @param numPaginas
	 * @param edicion
	 * @param codLibro         Codigo de incripcion asignado a cada Libro
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param fechaEdicion
	 * @param tapaDura
	 * @param sobrecubierta
	 * @param lgbtq
	 * @param prestado
	 */
	public void a�adirLibro(String titulo, String editorial, String nombreAutor, String apellidoAutor, int numPaginas,
			int edicion, int codLibro, double valoracionMedia, String fechaPublicacion, String fechaEdicion,
			boolean tapaDura, boolean sobrecubierta, boolean lgbtq, boolean prestado) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] == null) {
				libros[i] = new Libro(codLibro);
				libros[i].setTitulo(titulo);
				libros[i].setNombreAutor(nombreAutor);
				libros[i].setApellidoAutor(apellidoAutor);
				libros[i].setNumPaginas(numPaginas);
				libros[i].setEditorial(editorial);
				libros[i].setEdicion(edicion);
				libros[i].setValoracionMedia(valoracionMedia);
				libros[i].setFechaPublicacion(LocalDate.parse(fechaPublicacion));
				libros[i].setFechaEdicion(LocalDate.parse(fechaEdicion));
				libros[i].setTapaDura(tapaDura);
				libros[i].setSobrecubierta(sobrecubierta);
				libros[i].setLgbtq(lgbtq);
				libros[i].setPrestado(prestado);

				break;
			}
		}
	}

	// listar Libros
	/**
	 * Metodo para Listar los Libros
	 */
	public void listaLibros() {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				System.out.println(libros[i]);
			}
		}
	}

	// mostrar Libros
	/**
	 * Metodo para listar los libros con los datos b�sicos
	 */
	public void mostrarLibros() {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				System.out.println("Titulo : " + libros[i].getTitulo());
				System.out.println("Editorial : " + libros[i].getEditorial());
				System.out.println("Nombre del Autor : " + libros[i].getNombreAutor());
				System.out.println("Apellido del Autor : " + libros[i].getApellidoAutor());
				System.out.println("Valoraci�n Media : " + libros[i].getValoracionMedia());
				System.out.println("Fecha Publicaci�n : " + libros[i].getFechaPublicacion());
				System.out.println(" ");
			}
		}
	}

	// Buscar Libro por titulo
	/**
	 * Metodo para buscar un libro en concreto por su titulo
	 * 
	 * @param titulo
	 * @return
	 */
	public Libro buscarLibroTitulo(String titulo) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getTitulo().equalsIgnoreCase(titulo)) {
					return libros[i];
				}
			}
		}
		return null;
	}

	// Listar libros de un solo autor
	/**
	 * Antes del metodo listar por autor, he decidido crear un metodo para comprobar
	 * si ese autor existe en el programa
	 * 
	 * @param nombreAutor
	 * @param apellidoAutor
	 * @return
	 */
	public boolean existeAutor(String nombreAutor, String apellidoAutor) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getNombreAutor().equalsIgnoreCase(nombreAutor)
						&& libros[i].getApellidoAutor().equalsIgnoreCase(apellidoAutor)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Lista de libros de un Autor en concreto
	 * 
	 * @param nombreAutor
	 * @param apellidoAutor
	 */
	public void listLibrosAutor(String nombreAutor, String apellidoAutor) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getNombreAutor().equalsIgnoreCase(nombreAutor)
						&& libros[i].getApellidoAutor().equalsIgnoreCase(apellidoAutor)) {
					System.out.println("Titulo : " + libros[i].getTitulo());
					System.out.println("Editorial : " + libros[i].getEditorial());
					System.out.println("Nombre del Autor : " + libros[i].getNombreAutor());
					System.out.println("Apellido del Autor : " + libros[i].getApellidoAutor());
					System.out.println("Valoraci�n Media : " + libros[i].getValoracionMedia());
					System.out.println("Fecha Publicaci�n : " + libros[i].getFechaPublicacion());
					System.out.println(" ");
				}
			}
		}
	}

	// listar libros que tengan contenido LGBTQ
	/**
	 * Lista de libros con contenido del colectivo LGBTQ
	 * 
	 * @param lgbtq
	 */
	public void listLibrosColectivo(boolean lgbtq) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].isLgbtq() == lgbtq) {
					System.out.println("Titulo : " + libros[i].getTitulo());
					System.out.println("Editorial : " + libros[i].getEditorial());
					System.out.println("Nombre del Autor : " + libros[i].getNombreAutor());
					System.out.println("Apellido del Autor : " + libros[i].getApellidoAutor());
					System.out.println("Valoraci�n Media : " + libros[i].getValoracionMedia());
					System.out.println("Fecha Publicaci�n : " + libros[i].getFechaPublicacion());
					System.out.println(" ");
				}
			}
		}
	}

	// listar libros por el a�o

	/**
	 * Metodo para comprobar si el a�o introducido existe antes de usar el metodo de
	 * listar
	 * @param anio
	 * @return 
	 */
	public boolean existea�o(int anio) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getFechaEdicion().getYear() == anio) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que te da un lista de libros dependiendo del a�o introducido
	 * 
	 * @param a�o Parametro int que introduces para buscar por dicho a�o
	 */
	public void listaLibrosA�o(int anio) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getFechaPublicacion().getYear() == anio) {
					System.out.println("Titulo : " + libros[i].getTitulo());
					System.out.println("Editorial : " + libros[i].getEditorial());
					System.out.println("Nombre del Autor : " + libros[i].getNombreAutor());
					System.out.println("Apellido del Autor : " + libros[i].getApellidoAutor());
					System.out.println("Valoraci�n Media : " + libros[i].getValoracionMedia());
					System.out.println("Fecha Publicaci�n : " + libros[i].getFechaPublicacion());
					System.out.println(" ");
				}
			}
		}
	}

	// Modificar prestamo

	/*
	 * Metodo para comprobar si el titulo introducido existe antes de usar el metodo
	 * de modificar prestamo
	 */
	public boolean existeLibro(String titulo) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getTitulo().equalsIgnoreCase(titulo)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que comprueba que si los libros esta prestados o no
	 * 
	 * @param titulo
	 * @param prestado
	 * @return
	 */
	public boolean libroPrestado(String titulo) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getTitulo().equalsIgnoreCase(titulo)) {
					if (libros[i].isPrestado() == true) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que se encarga de modificar el estado de prestamo de un libro
	 * 
	 * @param codLibro
	 * @param prestado
	 */
	public void prestamoLibro(String titulo, boolean prestado) {
		for (int i = 0; i < libros.length; i++) {
			if (libros[i] != null) {
				if (libros[i].getTitulo().equalsIgnoreCase(titulo)) {
					libros[i].setPrestado(prestado);
				}
			}
		}
	}

	// ************************************************************************************************
	// METODOS NOVELAS
	// ************************************************************************************************

	// A�adir Novela
	/**
	 * Metodo para A�adir Novelas, usando el constructor completo de la clase Novela
	 * 
	 * @param codLibro
	 * @param titulo
	 * @param editorial
	 * @param nombreAutor
	 * @param apellidoAutor
	 * @param numPaginas
	 * @param edicion
	 * @param codLibro
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param fechaEdicion
	 * @param tapaDura
	 * @param sobrecubierta
	 * @param lgbtq
	 * @param prestado
	 * @param edadRecomendada
	 */
	public void a�adirNovela(String titulo, String editorial, String nombreAutor, String apellidoAutor, int numPaginas,
			int edicion, int codLibro, double valoracionMedia, String fechaPublicacion, String fechaEdicion,
			boolean tapaDura, boolean sobrecubierta, boolean lgbtq, boolean prestado, String genero,
			int edadRecomendada) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] == null) {
				novelas[i] = new Novela(genero);
				novelas[i].setCodLibro(codLibro);
				novelas[i].setTitulo(titulo);
				novelas[i].setNombreAutor(nombreAutor);
				novelas[i].setApellidoAutor(apellidoAutor);
				novelas[i].setNumPaginas(numPaginas);
				novelas[i].setEditorial(editorial);
				novelas[i].setEdicion(edicion);
				novelas[i].setValoracionMedia(valoracionMedia);
				novelas[i].setFechaPublicacion(LocalDate.parse(fechaPublicacion));
				novelas[i].setFechaEdicion(LocalDate.parse(fechaEdicion));
				novelas[i].setTapaDura(tapaDura);
				novelas[i].setSobrecubierta(sobrecubierta);
				novelas[i].setLgbtq(lgbtq);
				novelas[i].setPrestado(prestado);
				novelas[i].setEdadRecomendada(edadRecomendada);
				break;
			}
		}
	}

	// listar novelas
	/**
	 * Metodo para listar las Novelas
	 */
	public void listaNovelas() {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				System.out.println(novelas[i]);
			}
		}
	}

	// mostrar novelas datos basicos
	/**
	 * Metodo para mostrar las Novelas con los datos basicos
	 */
	public void mostrarNovelas() {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				System.out.println("Titulo : " + novelas[i].getTitulo());
				System.out.println("Editorial : " + novelas[i].getEditorial());
				System.out.println("Nombre del Autor : " + novelas[i].getNombreAutor());
				System.out.println("Apellido del Autor : " + novelas[i].getApellidoAutor());
				System.out.println("Valoraci�n Media : " + novelas[i].getValoracionMedia());
				System.out.println("Fecha Publicaci�n : " + novelas[i].getFechaPublicacion());
				System.out.println("G�nero Literario : " + novelas[i].getGenero());
				System.out.println("Edad Recomendada : " + novelas[i].getEdadRecomendada());
				System.out.println(" ");
			}
		}
	}

	// Bucar novela por titulo
	/**
	 * Metodo para buscar novela por Titulo
	 * 
	 * @param titulo
	 * @return
	 */
	public Novela buscarNovela(String titulo) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getTitulo().equalsIgnoreCase(titulo)) {
					return novelas[i];
				}
			}
		}
		return null;
	}

	// Listar novelas por autor
	/**
	 * Comprobacion de la existencia del autor
	 * 
	 * @param nombreAutor
	 * @param apellidoAutor
	 * @return
	 */
	public boolean existeAutorNovela(String nombreAutor, String apellidoAutor) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getNombreAutor().equalsIgnoreCase(nombreAutor)
						&& novelas[i].getApellidoAutor().equalsIgnoreCase(apellidoAutor)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo para listar novelas de un autor en concreto
	 * 
	 * @param nombreAutor
	 * @param apellidoAutor
	 */
	public void listaNovelaAutor(String nombreAutor, String apellidoAutor) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getNombreAutor().equalsIgnoreCase(nombreAutor)
						&& novelas[i].getApellidoAutor().equalsIgnoreCase(apellidoAutor)) {
					System.out.println("Titulo : " + novelas[i].getTitulo());
					System.out.println("Editorial : " + novelas[i].getEditorial());
					System.out.println("Nombre del Autor : " + novelas[i].getNombreAutor());
					System.out.println("Apellido del Autor : " + novelas[i].getApellidoAutor());
					System.out.println("Valoraci�n Media : " + novelas[i].getValoracionMedia());
					System.out.println("Fecha Publicaci�n : " + novelas[i].getFechaPublicacion());
					System.out.println("G�nero Literario : " + novelas[i].getGenero());
					System.out.println("Edad Recomendada : " + novelas[i].getEdadRecomendada());
					System.out.println(" ");
				}
			}
		}
	}

	/**
	 * Metodo que comprueba si el genero literario deseado existe
	 * @param genero
	 * @return
	 */
	// Listar novelas por genero
	public boolean existeGenero(String genero) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getGenero().equalsIgnoreCase(genero)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo para listar las novelas de un genero literario
	 * 
	 * @param genero
	 */
	public void listaNovelaGenero(String genero) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getGenero().equalsIgnoreCase(genero)) {
					System.out.println("Titulo : " + novelas[i].getTitulo());
					System.out.println("Editorial : " + novelas[i].getEditorial());
					System.out.println("Nombre del Autor : " + novelas[i].getNombreAutor());
					System.out.println("Apellido del Autor : " + novelas[i].getApellidoAutor());
					System.out.println("Valoraci�n Media : " + novelas[i].getValoracionMedia());
					System.out.println("Fecha Publicaci�n : " + novelas[i].getFechaPublicacion());
					System.out.println("G�nero Literario : " + novelas[i].getGenero());
					System.out.println("Edad Recomendada : " + novelas[i].getEdadRecomendada());
					System.out.println(" ");
				}
			}
		}
	}

	// listar novelas por edad
	/**
	 * Comprobaciond de la edad
	 * 
	 * @param edadRecomendada
	 * @return
	 */
	public boolean comprobarEdad(int edadRecomendada) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getEdadRecomendada() == edadRecomendada) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo para listar novelas por la Edad
	 * 
	 * @param edadRecomendada
	 */
	public void listaNovelasEdad(int edadRecomendada) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getEdadRecomendada() == edadRecomendada) {
					System.out.println("Titulo : " + novelas[i].getTitulo());
					System.out.println("Editorial : " + novelas[i].getEditorial());
					System.out.println("Nombre del Autor : " + novelas[i].getNombreAutor());
					System.out.println("Apellido del Autor : " + novelas[i].getApellidoAutor());
					System.out.println("Valoraci�n Media : " + novelas[i].getValoracionMedia());
					System.out.println("Fecha Publicaci�n : " + novelas[i].getFechaPublicacion());
					System.out.println("G�nero Literario : " + novelas[i].getGenero());
					System.out.println("Edad Recomendada : " + novelas[i].getEdadRecomendada());
					System.out.println(" ");
				}
			}
		}
	}

	// listar novelas por si tiene contenido LGBTQ
	/**
	 * Metodo listar las novelas con contenido del Colectivo
	 * 
	 * @param lgbtq
	 */
	public void listaNovelasContenido(boolean lgbtq) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].isLgbtq() == lgbtq) {
					System.out.println("Titulo : " + novelas[i].getTitulo());
					System.out.println("Editorial : " + novelas[i].getEditorial());
					System.out.println("Nombre del Autor : " + novelas[i].getNombreAutor());
					System.out.println("Apellido del Autor : " + novelas[i].getApellidoAutor());
					System.out.println("Valoraci�n Media : " + novelas[i].getValoracionMedia());
					System.out.println("Fecha Publicaci�n : " + novelas[i].getFechaPublicacion());
					System.out.println("G�nero Literario : " + novelas[i].getGenero());
					System.out.println("Edad Recomendada : " + novelas[i].getEdadRecomendada());
					System.out.println(" ");
				}
			}
		}
	}

	// Modificar prestamo
	/**
	 * Metodo que comprueba que si las novelas esta prestados o no
	 * 
	 * @param titulo
	 * @param prestado
	 * @return
	 */
	public boolean novelaPrestado(String titulo) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getTitulo().equalsIgnoreCase(titulo)) {
					if (novelas[i].isPrestado() == true) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Comprobar si la novela que se quiere prestar existe
	 * 
	 * @param titulo
	 * @return
	 */
	public boolean existeNovela(String titulo) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getTitulo().equalsIgnoreCase(titulo)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que se encarga de modificar el estado de prestamo de una novela
	 * 
	 * @param codLibro
	 * @param prestado
	 */
	public void prestamoNovela(String titulo, boolean prestado) {
		for (int i = 0; i < novelas.length; i++) {
			if (novelas[i] != null) {
				if (novelas[i].getTitulo().equalsIgnoreCase(titulo)) {
					novelas[i].setPrestado(prestado);
				}
			}
		}
	}

	// ************************************************************************************************
	// METODOS DVD's
	// ************************************************************************************************

	// A�adir Dvd
	/**
	 * Metodo para a�adir Dvd
	 * 
	 * @param titulo
	 * @param duracion
	 * @param codDvd           codigo de Dvd mediante el cual se introducen.
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param lgbtq
	 * @param prestado
	 */
	public void a�adirDvd(String titulo, int duracion, int codDvd, double valoracionMedia, String fechaPublicacion,
			boolean lgbtq, boolean prestado) {
		for (int i = 0; i < dvds.length; i++) {
			if (dvds[i] == null) {
				dvds[i] = new Dvd(codDvd);
				dvds[i].setTitulo(titulo);
				dvds[i].setDuracion(duracion);
				dvds[i].setValoracionMedia(valoracionMedia);
				dvds[i].setFechaPublicacion(LocalDate.parse(fechaPublicacion));
				dvds[i].setLgbtq(lgbtq);
				dvds[i].setPrestado(prestado);
				break;
			}
		}
	}

	// listar Dvd
	/**
	 * Metodo para realizar listas de Dvds
	 */
	public void listarDvds() {
		for (int i = 0; i < dvds.length; i++) {
			if (dvds[i] != null) {
				System.out.println(dvds[i]);
			}
		}
	}

	// Buscar Dvd por titulo
	/**
	 * Metodo para buscar un Dvd por el titulo
	 * 
	 * @param titulo
	 * @return
	 */
	public Dvd buscarDvd(String titulo) {
		for (int i = 0; i < dvds.length; i++) {
			if (dvds[i] != null) {
				if (dvds[i].getTitulo().equalsIgnoreCase(titulo)) {
					return dvds[i];
				}
			}
		}
		return null;
	}

	// Listar dvd del colectivo
	/**
	 * Metodo para ver una lista de dvds con contenido lgbtq
	 * 
	 * @param lgbtq
	 */
	public void listDvdsColectivo(boolean lgbtq) {
		for (int i = 0; i < dvds.length; i++) {
			if (dvds[i] != null) {
				if (dvds[i].isLgbtq() == lgbtq) {
					System.out.println(dvds[i]);
				}
			}
		}
	}

	// Modificar prestamo
	/**
	 * Comprobar si el dvd que se quiere prestar existe
	 * 
	 * @param titulo
	 * @return
	 */
	public boolean existeDvd(String titulo) {
		for (int i = 0; i < dvds.length; i++) {
			if (dvds[i] != null) {
				if (dvds[i].getTitulo().equalsIgnoreCase(titulo)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que comprueba que si los dvd esta prestados o no
	 * 
	 * @param titulo
	 * @param prestado
	 * @return
	 */
	public boolean dvdPrestado(String titulo) {
		for (int i = 0; i < dvds.length; i++) {
			if (dvds[i] != null) {
				if (dvds[i].getTitulo().equalsIgnoreCase(titulo)) {
					if (dvds[i].isPrestado() == true) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que se encarga de modificar el estado de prestamo de un dvd
	 * 
	 * @param codLibro
	 * @param prestado
	 */
	public void prestamoDvd(String titulo, boolean prestado) {
		for (int i = 0; i < dvds.length; i++) {
			if (dvds[i] != null) {
				if (dvds[i].getTitulo().equalsIgnoreCase(titulo)) {
					dvds[i].setPrestado(prestado);
				}
			}
		}
	}

	// ************************************************************************************************
	// METODOS CD's
	// ************************************************************************************************

	// Metodo a�adir Cd
	/**
	 * Metodo para a�adir Cd
	 * 
	 * @param titulo
	 * @param artista
	 * @param duracion
	 * @param codCd            Codigo por el que se introducen los cds
	 * @param valoracionMedia
	 * @param fechaPublicacion
	 * @param prestado
	 */
	public void a�adirCd(String titulo, String artista, int duracion, int codCd, double valoracionMedia,
			String fechaPublicacion, boolean prestado) {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] == null) {
				cds[i] = new Cd(codCd);
				cds[i].setTitulo(titulo);
				cds[i].setArtista(artista);
				cds[i].setDuracion(duracion);
				cds[i].setValoracionMedia(valoracionMedia);
				cds[i].setFechaPublicacion(LocalDate.parse(fechaPublicacion));
				cds[i].setPrestado(prestado);
				break;
			}
		}
	}

	// metodo listar cds
	/**
	 * Metodo para listar cds
	 */
	public void listarCds() {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] != null) {
				System.out.println(cds[i]);
			}
		}
	}

	// listar cd por artista
	/**
	 * Metodo que comprueba la existencia del programa o grupo en el programa
	 * 
	 * @return
	 */
	public boolean existeArtista(String artista) {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] != null) {
				if (cds[i].getArtista().equalsIgnoreCase(artista)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo para listar cd por el artista
	 * 
	 * @param artista
	 */
	public void listarCdArtista(String artista) {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] != null) {
				if (cds[i].getArtista().equalsIgnoreCase(artista)) {
					System.out.println(cds[i]);
				}
			}
		}
	}

	// buscar cd por titulo
	/**
	 * Metodo para buscar un cd por su titulo
	 * 
	 * @param titulo
	 * @return
	 */
	public Cd buscarCd(String titulo) {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] != null) {
				if (cds[i].getTitulo().equalsIgnoreCase(titulo)) {
					return cds[i];
				}
			}
		}
		return null;
	}

	// Modificar prestamo
	/**
	 * Comprobar si el dvd que se quiere prestar existe
	 * 
	 * @param titulo
	 * @return
	 */
	public boolean existeCd(String titulo) {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] != null) {
				if (cds[i].getTitulo().equalsIgnoreCase(titulo)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que comprueba que si los cds estan prestados o no
	 * 
	 * @param titulo
	 * @param prestado
	 * @return
	 */
	public boolean cdPrestado(String titulo) {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] != null) {
				if (cds[i].getTitulo().equalsIgnoreCase(titulo)) {
					if (cds[i].isPrestado() == true) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Metodo que se encarga de modificar el estado de prestamo de un dvd
	 * 
	 * @param codLibro
	 * @param prestado
	 */
	public void prestamoCd(String titulo, boolean prestado) {
		for (int i = 0; i < cds.length; i++) {
			if (cds[i] != null) {
				if (cds[i].getTitulo().equalsIgnoreCase(titulo)) {
					cds[i].setPrestado(prestado);
				}
			}
		}
	}

	// ************************************************************************************************
	// METODOS SOCIO
	// ************************************************************************************************
	// Alta Socios
	/**
	 * Metodo para dar de Alta a los Socios
	 * 
	 * @param nombre
	 * @param apellidos
	 * @param numSocio
	 * @param edad
	 * @param multaExtravioDa�o
	 * @param multaRetrasoDevolucion
	 * @param diasRetraso
	 */
	public void darAltaSocio(String nombre, String apellidos, int numSocio, String fechaNaciento, int edad,
			double multaExtravioDanio, double multaRetrasoDevolucion, int diasRetraso) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i] == null) {
				socios[i] = new Socio(numSocio);
				socios[i].setNombre(nombre);
				socios[i].setApellidos(apellidos);
				socios[i].setFechaNacimiento(LocalDate.parse(fechaNaciento));
				socios[i].setEdad(edad);
				socios[i].setMultaExtravioDanio(multaExtravioDanio);
				socios[i].setMultaRetrasoDevolucion(multaRetrasoDevolucion);
				socios[i].setDiasRetraso(diasRetraso);
				break;
			}
		}
	}

	// Metodo listar Socios
	/**
	 * Metodo para listar los Socios
	 */
	public void listarSocios() {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i] != null) {
				System.out.println(socios[i]);
			}
		}
	}

	// Metodo Mostrar socios con los atributos basicos
	/**
	 * Metodo para mostrar Los socios unicamente con los datos imprescindibles
	 */
	public void mostrarSocios() {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i] != null) {
				System.out.println("Nombre : " + socios[i].getNombre());
				System.out.println("Apellido : " + socios[i].getApellidos());
				System.out.println("Edad : " + socios[i].getEdad());
				System.out.println("N�mero de Socio : " + socios[i].getNumSocio());
				System.out.println(" ");
			}
		}
	}

	// Buscar Socio
	/**
	 * Metodo Buscar Socio
	 * 
	 * @param numSocio
	 * @return
	 */
	public Socio buscarSocio(int numSocio) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i].getNumSocio() == numSocio) {
				return socios[i];
			}
		}
		return null;
	}

	// Comprobar la existencia de un socio
	/**
	 * Metodo que comprueba la existencia de un socio por su numero de socio
	 * @param numSocio
	 * @return true si el socio existe
	 */
	public boolean existeSocio(int numSocio) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i] != null) {
				if (socios[i].getNumSocio() == numSocio) {
					return true;
				}
			}
		}
		return false;
	}

	// Borrar Socio
	/**
	 * Metodo para borrar socio
	 * 
	 * @param numSocio
	 */
	public void borrarSocio(int numSocio) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i].getNumSocio() == numSocio) {
				socios[i] = null;
			}
		}
	}

	// Modificar Datos Socio
	/**
	 * Metodo para modificar la multa de retraso
	 * 
	 * @param numSocio
	 * @param multaRetrasoDevolucion
	 * @param diasRetraso
	 */
	public void modificarSocioRetraso(int numSocio, double multaRetrasoDevolucion, int diasRetraso) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i] != null) {
				if (socios[i].getNumSocio() == numSocio) {
					socios[i].setMultaRetrasoDevolucion(multaRetrasoDevolucion);
					socios[i].setDiasRetraso(diasRetraso);
				}
			}
		}
	}

	/**
	 * Metodo que llama al metodo que calcula el precio final por retraso en la
	 * devoluci�n
	 * @param numSocio
	 */
	public void devolverMultaTotalRetraso(int numSocio) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i] != null) {
				if (socios[i].getNumSocio() == numSocio) {
					System.out.println(socios[i].obtenerMultaTotalRetraso());
				}
			}
		}

	}

	/**
	 * Metodo para mudificar la multa por perdida o da�o
	 * 
	 * @param numSocio
	 * @param multaExtravioDa�o
	 */
	public void modificarSocioDa�o(int numSocio, double multaExtravioDanio) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i].getNumSocio() == numSocio) {
				socios[i].setMultaExtravioDanio(multaExtravioDanio);

			}
		}
	}

	/**
	 * Metodo que llama al metodo que calcula el precio final de la multa por da�o o
	 * perdida
	 * 
	 * @param numSocio
	 */
	public void devolverMultaTotalDanioPerdida(int numSocio) {
		for (int i = 0; i < socios.length; i++) {
			if (socios[i] != null) {
				if (socios[i].getNumSocio() == numSocio) {
					System.out.println(socios[i].obtenerMultaTotalDanio());
				}
			}
		}
	}

}
