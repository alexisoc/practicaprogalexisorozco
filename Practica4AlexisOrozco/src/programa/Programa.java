package programa;

import java.util.Scanner;

import clases.Biblioteca;

public class Programa {
	/**
	 * 
	 * @author AlexisOrozco
	 * @version 1
	 *
	 */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

		// Crear la biblioteca con los Libros, novelas, cuentos y socios
		int maxLibros = 4;
		int maxNovelas = 5;
		int maxDvds = 5;
		int maxCds = 5;
		int maxSocios = 5;

		Biblioteca biblioteca01 = new Biblioteca(maxLibros, maxNovelas, maxDvds, maxCds, maxSocios);

		/**
		 * Llamar al metodo que introduce los datos de los array de objetos
		 */
		MetodosPrograma.poblar(biblioteca01);

		/**
		 * Metodo que solicita el usuario y contrase�a al iniciar el programa. Estos
		 * datos son irrelevantes, es decir no son nada en especifico, se utilizan en la
		 * opcion 7 del men� cuando vuelve a solicitarlo y deben coincidir con los
		 * introducidos al iniciar el programa
		 */
		int opcion = 0;
		String usuario = MetodosPrograma.usuario();
		String clave = MetodosPrograma.contrasenia();
		/**
		 * Llamada al metodo menu principal
		 */
		MetodosPrograma.menuPrincipal(opcion, biblioteca01, usuario, clave);

		input.close();

	}
	
}
