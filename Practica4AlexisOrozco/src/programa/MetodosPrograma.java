package programa;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

import clases.Biblioteca;

public class MetodosPrograma {
	static Scanner input = new Scanner(System.in);

	/**
	 * Metodo Para poblar los arrays de objetos de la clase Biblioteca
	 * 
	 * @param biblioteca01
	 */
	static void poblar(Biblioteca biblioteca01) {
		// añadir 4 Libros
		/**
		 * Añadimos los Libros
		 */
		biblioteca01.añadirLibro("Animal Spirit", "HarperOne", "Kim", "Krans", 207, 3, 1, 5.0, ("2018-03-15"),
				("2020-05-20"), false, false, false, false);
		biblioteca01.añadirLibro("La Paleta Perfecta", "PromoPress", "Kim", "Krans", 320, 1, 2, 3.5, ("2018-02-20"),
				("2018-02-20"), true, false, true, false);
		biblioteca01.añadirLibro("La informacion en el Diseño", "Parramon", "Isabelle", "Meirelles", 224, 1, 3, 2.0,
				("2014-05-29"), ("2018-05-29"), false, false, false, false);
		biblioteca01.añadirLibro("ABC Dream", "PromoPress", "Kim", "Krans", 48, 1, 4, 5.0, ("2016-10-10"),
				("2016-10-10"), true, false, true, false);

		// añadir 5 Novelas
		/**
		 * Añadimos las Novelas
		 */
		biblioteca01.añadirNovela("Las ventajas de ser un Marginado", "Alfaguara", "Stephen", "Chbosky", 264, 20, 11,
				5.0, ("1999-02-01"), ("2020-12-17"), false, false, true, false, "Juvenil", 15);
		biblioteca01.añadirNovela("Amigo imaginario", "Planeta", "Stephen", "Chbosky", 816, 2, 12, 3.0, ("2019-02-01"),
				("2020-12-17"), false, false, true, false, "Juvenil", 20);
		biblioteca01.añadirNovela("Rojo, Blanco y Sangre Azul", "Alfaguara", "Stephen", "Chbosky", 264, 20, 11, 5.0,
				("1999-12-06"), ("2019-09-07"), false, false, true, false, "Terror", 15);
		biblioteca01.añadirNovela("IT", "HarperOne", "Stephen", "King", 264, 20, 13, 5.0, ("2020-05-27"),
				("2018-11-01"), false, false, false, false, "Terror", 20);
		biblioteca01.añadirNovela("Ojos de fuego", "PromoPress", "Stephen", "King", 264, 20, 14, 5.0, ("1999-11-11"),
				("2019-10-20"), false, false, false, false, "Juvenil", 15);

		// añadir 5 Dvds
		/**
		 * Añadimos los Dvd
		 */
		biblioteca01.añadirDvd("Pinocho", 135, 1, 2.5, ("2019-02-01"), false, false);
		biblioteca01.añadirDvd("Buscando a Dory", 90, 2, 5.0, ("2020-05-27"), false, false);
		biblioteca01.añadirDvd("Love Simon", 150, 3, 0.5, ("2018-02-20"), true, false);
		biblioteca01.añadirDvd("Alex Strangelove", 75, 4, 1.2, ("2019-10-20"), true, false);
		biblioteca01.añadirDvd("Cherry Magic", 120, 5, 4.7, ("2014-05-29"), true, false);

		// añadir 5 Cds
		/**
		 * Añadimos los Cd
		 */
		biblioteca01.añadirCd("Red", "Taylor Swift", 180, 1, 4.5, ("2014-05-29"), false);
		biblioteca01.añadirCd("7 fates", "BTS", 200, 2, 5.0, ("2011-05-29"), false);
		biblioteca01.añadirCd("Be", "BTS", 240, 3, 5.0, ("2020-05-29"), false);
		biblioteca01.añadirCd("Noeasy", "Stray Kids", 135, 4, 5.0, ("2021-05-29"), false);
		biblioteca01.añadirCd("Butter", "BTS", 120, 5, 5.0, ("2021-12-31"), false);

		// añadir 5 Socios
		/**
		 * Damos de alta a los Socios
		 */
		biblioteca01.darAltaSocio("Pepe", "Perez", 1, "2004-01-01", 18, 0.0, 0.0, 0);
		biblioteca01.darAltaSocio("Pepa", "Pinzon", 2, "2002-01-01", 20, 0.0, 0.0, 0);
		biblioteca01.darAltaSocio("Manolo", "Martinez", 3, "1999-01-01", 23, 0.0, 0.0, 0);
		biblioteca01.darAltaSocio("Fernando", "Fernandez", 4, "1988-01-01", 34, 0.0, 0.0, 0);
		biblioteca01.darAltaSocio("Benito", "Berdejo", 5, "2007-01-01", 15, 0.0, 0.0, 0);
	}

	/**
	 * Metodo para crear un usuario
	 * 
	 * @return String, nombre de usuario introducido
	 */
	public static String usuario() {
		System.out.println("Introduzca su Usuario");
		String nUsuario = input.nextLine();
		return nUsuario;

	}

	/**
	 * Metodo para crear contraseña
	 * 
	 * @return String, contraseña introducida
	 */
	public static String contrasenia() {
		System.out.println("Introduzca su contraseña");
		String uContrasenia = input.nextLine();
		return uContrasenia;
	}

	// ************************************************************************************************
	// MENU PRINCIPAL
	// ************************************************************************************************
	/**
	 * Menu Principal del programa
	 * 
	 * @param opcion
	 * @param biblioteca01
	 */
	static void menuPrincipal(int opcion, Biblioteca biblioteca01, String name, String clave) throws NumberFormatException{
		do {
			System.out.println("               *****                        *****                 ");
			System.out.println("               **    ****               ****   **                 ");
			System.out.println("               **       ****        ****       **                 ");
			System.out.println("               **           ********           **                 ");
			System.out.println("               **              **              **                 ");
			System.out.println("               **              **              **                 ");
			System.out.println("               **              **              **                 ");
			System.out.println("               **              **              **                 ");
			System.out.println("               **              **              **                 ");
			System.out.println("               **              **              **                 ");
			System.out.println("               **              **              **                 ");
			System.out.println("               **          **********          **                 ");
			System.out.println("               **     *****         *****      **                 ");
			System.out.println("               ********                **********                 ");
			System.out.println("******************************************************************");
			System.out.println("*                         Menu Principal                         *");
			System.out.println("******************************************************************");
			System.out.println("1.- Datos Libros");
			System.out.println("2.- Datos Novelas");
			System.out.println("3.- Datos Dvd");
			System.out.println("4.- Datos Cd");
			System.out.println("5.- Ver lista de Socios");
			System.out.println("6.- SECCIÓN DE ADMINISTRADOR");
			System.out.println("7.- SALIR");
			System.out.println("******************************************************************");
			opcion = comprobarNumero(opcion);
			
			
			
			
			input.nextLine();
			switch (opcion) {

			case 1:
				menuLibros(opcion, biblioteca01);
				break;
			case 2:
				menuNovelas(opcion, biblioteca01);
				break;
			case 3:
				menuDvd(opcion, biblioteca01);
				break;
			case 4:
				menuCd(opcion, biblioteca01);
				break;
			case 5:
				biblioteca01.mostrarSocios();
				break;
			case 6:
				menuAdmin(opcion, biblioteca01, name, clave);
				break;
			case 7:
				System.out.println("Fin del programa");
				cerrarScanner();
				break;

			default:
				System.out.println("*************************");
				System.out.println("* Opción no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}

		} while (opcion != 7);

	}

	private static int comprobarNumero(int opcion) {
		try {
			opcion = input.nextInt();
		} catch(java.util.InputMismatchException e) {
			System.out.println("No es un número " + e );
		}
		return opcion;
	}

	/**
	 * Metodo para cerrar el Scanner
	 */
	private static void cerrarScanner() {
		input.close();
	}

	// ************************************************************************************************
	// MENU LIBROS
	// ************************************************************************************************
	/**
	 * Menu para acceder a los metodos de los Libros
	 * 
	 * @param opcion
	 * @param biblioteca01
	 */
	static void menuLibros(int opcion, Biblioteca biblioteca01) {
		do {
			System.out.println("******************************************************************");
			System.out.println("*                           Menu Libros                          *");
			System.out.println("******************************************************************");
			System.out.println("1.- Lista Libros");
			System.out.println("2.- Buscar Libro por Titulo");
			System.out.println("3.- Buscar los Libros de un Autor");
			System.out.println("4.- Lista de libros con contenido LGBTQ+");
			System.out.println("5.- Buscar los Libros de un año");
			System.out.println("6.- Marcar un libro como prestado");
			System.out.println("7.- Marcar un libro como devuelto");
			System.out.println("8.- SALIR");
			System.out.println("******************************************************************");
			opcion = input.nextInt();
			input.nextLine();
			String titulo;
			int anyo;
			switch (opcion) {

			case 1:
				biblioteca01.mostrarLibros();
				break;
			case 2:
				System.out.println("Introduce el nombre del libro");
				titulo = input.nextLine();
				if (biblioteca01.buscarLibroTitulo(titulo) == null) {
					System.out.println("El Libro introducido no se encuentra en esta Biblioteca");
				} else {
					System.out.println(biblioteca01.buscarLibroTitulo(titulo));
				}
				break;
			case 3:
				System.out.println("Introduce el nombre del Autor");
				String nombre = input.nextLine();
				System.out.println("Introduce el apellido del Autor");
				String apellido = input.nextLine();
				if (biblioteca01.existeAutor(nombre, apellido) == false) {
					System.out.println("El nombre o apellido introducidos no corresponden");
				} else {

					biblioteca01.listLibrosAutor(nombre, apellido);
				}
				input.nextLine();
				break;
			case 4:
				biblioteca01.listLibrosColectivo(true);
				break;
			case 5:
				System.out.println("Introduce el año");
				anyo = input.nextInt();
				input.nextLine();
				if (biblioteca01.existeaño(anyo) == false) {
					System.out.println("No hay ningun libro en la biblioteca Publicado en el año dado");
				} else {
					biblioteca01.listaLibrosAño(anyo);
				}
				break;
			case 6:
				System.out.println("Da el titulo del libro que se ha prestado");
				titulo = input.nextLine();
				if (biblioteca01.existeLibro(titulo) == false) {
					System.out.println("El Libro introducido no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.libroPrestado(titulo) == true) {
						System.out.println("El libro ya está prestado");
					} else {
						biblioteca01.prestamoLibro(titulo, true);
						System.out.println(biblioteca01.buscarLibroTitulo(titulo));
					}
				}
				break;
			case 7:
				System.out.println("Da el titulo del libro que se ha devuelto");
				titulo = input.nextLine();
				if (biblioteca01.existeLibro(titulo) == false) {
					System.out.println("El Libro introducido no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.libroPrestado(titulo) == false) {
						System.out.println("El libro no está prestado");
					} else {
						biblioteca01.prestamoLibro(titulo, false);
						System.out.println(biblioteca01.buscarLibroTitulo(titulo));
					}
				}
				break;
			case 8:
				System.out.println("Volver a Menu Principal");
				System.out.println(" ");
				break;
			default:
				System.out.println("*************************");
				System.out.println("* Opción no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}

		} while (opcion != 8);

	}

	// ************************************************************************************************
	// MENU NOVELAS
	// ************************************************************************************************
	/**
	 * Menú para las Novelas
	 * 
	 * @param opcion
	 * @param biblioteca01
	 */
	private static void menuNovelas(int opcion, Biblioteca biblioteca01) {
		do {
			System.out.println("******************************************************************");
			System.out.println("*                           Menu Novelas                         *");
			System.out.println("******************************************************************");
			System.out.println("1.- Lista Novelas");
			System.out.println("2.- Buscar Novela por Titulo");
			System.out.println("3.- Buscar las Novelas de un Autor");
			System.out.println("4.- Lista de Novelas con contenido LGBTQ+");
			System.out.println("5.- Buscar Novelas por edad Recomendad");
			System.out.println("6.- Buscar novelas por Genero Literario");
			System.out.println("7.- Marcar una novela como prestada");
			System.out.println("8.- Marcar una novela como devuelta");
			System.out.println("9.- SALIR");
			System.out.println("******************************************************************");
			opcion = input.nextInt();
			input.nextLine();

			switch (opcion) {
			case 1:
				biblioteca01.mostrarNovelas();
				break;
			case 2:
				System.out.println("Introduce el nombre de la novela");
				String titulo = input.nextLine();
				if (biblioteca01.buscarNovela(titulo) == null) {
					System.out.println("La novela introducida no se encuentra en esta Biblioteca");
				} else {
					System.out.println(biblioteca01.buscarNovela(titulo));
				}
				break;
			case 3:
				System.out.println("Introduce el nombre del Autor");
				String nombre = input.nextLine();
				System.out.println("Introduce el apellido del Autor");
				String apellido = input.nextLine();
				if (biblioteca01.existeAutorNovela(nombre, apellido) == false) {
					System.out.println("El nombre o apellido introducidos no corresponden");
				} else {
					biblioteca01.listaNovelaAutor(nombre, apellido);
				}
				input.nextLine();
				break;
			case 4:
				biblioteca01.listaNovelasContenido(true);
				break;
			case 5:
				System.out.println("Introduce el año");
				int edad = input.nextInt();
				input.nextLine();
				if (biblioteca01.comprobarEdad(edad) == false) {

					System.out.println("No hay ningun libro en la biblioteca para esa Edad");
				} else {
					biblioteca01.listaNovelasEdad(edad);
				}
				break;
			case 6:
				System.out.println("Introduce el Género Literario");
				String literatura = input.nextLine();
				if (biblioteca01.existeGenero(literatura) == false) {
					System.out.println("No contamos actualmente con ninguna Novela del género solicitado");
				} else {
					biblioteca01.listaNovelaGenero(literatura);
				}
				break;
			case 7:
				System.out.println("Da el titulo de la novela que se ha prestado");
				titulo = input.nextLine();
				if (biblioteca01.existeNovela(titulo) == false) {
					System.out.println("La novela introducida no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.novelaPrestado(titulo) == true) {
						System.out.println("La novela ya está prestada");
					} else {
						biblioteca01.prestamoNovela(titulo, true);
						System.out.println(biblioteca01.buscarNovela(titulo));
					}
				}
				break;
			case 8:
				System.out.println("Da el titulo de la novela que se ha devuelto");
				titulo = input.nextLine();
				if (biblioteca01.existeNovela(titulo) == false) {
					System.out.println("La novela introducida no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.novelaPrestado(titulo) == false) {
						System.out.println("La novela no está prestada");
					} else {
						biblioteca01.prestamoNovela(titulo, false);
						System.out.println(biblioteca01.buscarNovela(titulo));
					}
				}
				break;
			case 9:
				System.out.println("Volver a menú principal");
				System.out.println(" ");
				break;
			default:
				System.out.println("*************************");
				System.out.println("* Opción no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}
		} while (opcion != 9);

	}

	// ************************************************************************************************
	// MENU DVD's
	// ************************************************************************************************
	/**
	 * Menu de Dvd's
	 * 
	 * @param opcion
	 * @param biblioteca01
	 */
	private static void menuDvd(int opcion, Biblioteca biblioteca01) {
		do {
			System.out.println("******************************************************************");
			System.out.println("*                             Menu Dvd                           *");
			System.out.println("******************************************************************");
			System.out.println("1.- Lista de Dvd's");
			System.out.println("2.- Buscar Dvd por Titulo");
			System.out.println("3.- Lista de Dvd's con contenido LGBTQ+");
			System.out.println("4.- Marcar un Dvd como prestado");
			System.out.println("5.- Marcar un Dvd como devuelto");
			System.out.println("6.- SALIR");
			System.out.println("******************************************************************");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				biblioteca01.listarDvds();
				break;
			case 2:
				System.out.println("Introduce el nombre del Dvd/Película");
				String titulo = input.nextLine();
				if (biblioteca01.buscarDvd(titulo) == null) {
					System.out.println("El Dvd/Pelicula introducido no se encuentra en esta Biblioteca");
				} else {
					System.out.println(biblioteca01.buscarDvd(titulo));
				}
				break;
			case 3:
				biblioteca01.listDvdsColectivo(true);
				break;
			case 4:
				System.out.println("Da el titulo del Dvd que se ha prestado");
				titulo = input.nextLine();
				if (biblioteca01.existeDvd(titulo) == false) {
					System.out.println("El Dvd introducido no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.dvdPrestado(titulo) == true) {
						System.out.println("El dvd ya está prestado");
					} else {
						biblioteca01.prestamoDvd(titulo, true);
						System.out.println(biblioteca01.buscarDvd(titulo));
					}
				}
				break;
			case 5:
				System.out.println("Da el titulo del Dvd que se ha devuelto");
				titulo = input.nextLine();
				if (biblioteca01.existeDvd(titulo) == false) {
					System.out.println("El Dvd introducido no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.dvdPrestado(titulo) == false) {
						System.out.println("El dvd no está prestado");
					} else {
						biblioteca01.prestamoDvd(titulo, false);
						System.out.println(biblioteca01.buscarDvd(titulo));
					}
				}
				break;
			case 6:
				System.out.println("Volver a menú principal");
				System.out.println(" ");
				break;
			default:
				System.out.println("*************************");
				System.out.println("* Opción no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}
		} while (opcion != 6);
	}

	// ************************************************************************************************
	// MENU CD's
	// ************************************************************************************************
	/**
	 * Menu Cd's
	 * 
	 * @param opcion
	 * @param biblioteca01
	 */
	private static void menuCd(int opcion, Biblioteca biblioteca01) {
		do {
			System.out.println("******************************************************************");
			System.out.println("*                             Menu Cd                            *");
			System.out.println("******************************************************************");
			System.out.println("1.- Lista de Cd's");
			System.out.println("2.- Buscar Cd por Titulo");
			System.out.println("3.- Lista de Cd's por artista o grupo");
			System.out.println("4.- Marcar un Cd como prestado");
			System.out.println("5.- Marcar un Cd como devuelto");
			System.out.println("6.- SALIR");
			System.out.println("******************************************************************");
			opcion = input.nextInt();
			input.nextLine();
			switch (opcion) {
			case 1:
				biblioteca01.listarCds();
				break;
			case 2:
				System.out.println("Introduce el nombre del Cd");
				String titulo = input.nextLine();
				if (biblioteca01.buscarCd(titulo) == null) {
					System.out.println("El Cd introducido no se encuentra en esta Biblioteca");
				} else {
					System.out.println(biblioteca01.buscarCd(titulo));
				}
				break;
			case 3:
				System.out.println("Introduce el Nombre del Artista o Grupo");
				String artista = input.nextLine();
				if (biblioteca01.existeArtista(artista) == false) {
					System.out.println("No contamos actualmente con ningún Cd de ese artista o grupo");
				} else {
					biblioteca01.listarCdArtista(artista);
				}
				break;
			case 4:
				System.out.println("Da el titulo del Cd que se ha prestado");
				titulo = input.nextLine();
				if (biblioteca01.existeCd(titulo) == false) {
					System.out.println("El Cd introducido no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.cdPrestado(titulo) == true) {
						System.out.println("El cd ya está prestado");
					} else {
						biblioteca01.prestamoCd(titulo, true);
						System.out.println(biblioteca01.buscarCd(titulo));
					}
				}
				break;
			case 5:
				System.out.println("Da el titulo del Cd que se ha devuelto");
				titulo = input.nextLine();
				if (biblioteca01.existeCd(titulo) == false) {
					System.out.println("El Cd introducido no se encuentra en esta Biblioteca");
				} else {
					if (biblioteca01.cdPrestado(titulo) == false) {
						System.out.println("El cd no está prestado");
					} else {
						biblioteca01.prestamoCd(titulo, false);
						System.out.println(biblioteca01.buscarCd(titulo));
					}
				}
				break;
			case 6:
				System.out.println("Volver a menú principal");
				System.out.println(" ");
				break;
			default:
				System.out.println("*************************");
				System.out.println("* Opción no comprendida *");
				System.out.println("*************************");
				System.out.println(" ");
				break;
			}
		} while (opcion != 6);
	}

	// ************************************************************************************************
	// MENU ADMINISTRADOR
	// ************************************************************************************************
	/**
	 * Menu de administrador comprobar los datos completos de los Libros comprobar
	 * los datos comletos de las novelas comprobar los datos completos de los socios
	 * borrar socio añadir un socio modificar deuda daño o rotura modificar deuda
	 * retraso
	 * 
	 * @param opcion
	 * @param biblioteca01
	 * @param name
	 * @param clave
	 */
	private static void menuAdmin(int opcion, Biblioteca biblioteca01, String name, String clave) {
		System.out.println("Introduce Nombre de Usuario: ");
		String usuario = input.nextLine();
		System.out.println("Introduce Contraseña: ");
		String contrasenia = input.nextLine();
		int numero;
		String nombre;
		String apellidos;
		LocalDate date;
		String fecha;
		int edad;
		if (usuario.equals(name) && contrasenia.equals(clave)) {
			do {
				System.out.println("******************************************************************");
				System.out.println("*                        Menu Administrador                      *");
				System.out.println("******************************************************************");
				System.out.println("1.- Comprobar datos Libros");
				System.out.println("2.- Comprobar datos Novelas");
				System.out.println("3.- Comprobar datos Socios");
				System.out.println("4.- Buscar Socio");
				System.out.println("5.- Borrar Socio");
				System.out.println("6.- Dar alta al socio 6");
				System.out.println("7.- Modificar deuda Rotura o Daño");
				System.out.println("8.- Modificar deuda Retraso de devolución");
				System.out.println("9.- SALIR");
				System.out.println("******************************************************************");
				opcion = input.nextInt();
				input.nextLine();
				switch (opcion) {
				case 1:
					biblioteca01.listaLibros();
					break;
				case 2:
					biblioteca01.listaNovelas();
					break;
				case 3:
					biblioteca01.listarSocios();
					break;
				case 4:
					System.out.println("Da el numero del Socio a busar");
					numero = input.nextInt();
					input.nextLine();
					if (biblioteca01.buscarSocio(numero) == null) {
						System.out.println("El Socio indicado no existe");
					} else {
						System.out.println(biblioteca01.buscarSocio(numero));
					}
					break;
				case 5:
					System.out.println("Da el número del Usuario a Eliminar");
					numero = input.nextInt();
					input.nextLine();
					if (biblioteca01.existeSocio(numero) == false) {
						System.out.println("El Socio indicado no existe");
					} else {

						biblioteca01.borrarSocio(numero);
						System.out.println("Lista socios modificada");
						biblioteca01.listarSocios();
					}
					break;
				case 6:
					System.out.println("Introduce el nombre del nuevo socio");
					nombre = input.nextLine();
					System.out.println("Introduce minimo un apellido del nuevo socio");
					apellidos = input.nextLine();
					System.out.println("Introduce la fecha de nacimiento del nuevo socio");
					fecha = input.nextLine();
					date = LocalDate.parse(fecha);
					Period anios = Period.between(date, LocalDate.now());
					edad = anios.getYears();
					biblioteca01.darAltaSocio(nombre, apellidos, 6, fecha, edad, 0.0, 0.0, 0);
					biblioteca01.listarSocios();
					break;
				case 7:
					System.out.println("Da el número del Usuario al que modificar la deuda");
					numero = input.nextInt();
					System.out.println("Da el valor inicial de la multa a aplicar");
					double valor = input.nextDouble();
					input.nextLine();
					if (biblioteca01.existeSocio(numero) == false) {
						System.out.println("El Socio indicado no existe");
					} else {
						biblioteca01.modificarSocioDaño(numero, valor);
						System.out.println(biblioteca01.buscarSocio(numero));
						biblioteca01.devolverMultaTotalDanioPerdida(numero);
					}
					break;
				case 8:
					System.out.println("Da el número del Usuario al que modificar la deuda");
					numero = input.nextInt();
					System.out.println("Da el valor inicial de la multa a aplicar");
					valor = input.nextDouble();
					System.out.println("Días que lleva de retraso");
					int dias = input.nextInt();
					input.nextLine();
					if (biblioteca01.existeSocio(numero) == false) {
						System.out.println("El Socio indicado no existe");
					} else {
						biblioteca01.modificarSocioRetraso(numero, valor, dias);
						System.out.println(biblioteca01.buscarSocio(numero));
						biblioteca01.devolverMultaTotalRetraso(numero);
					}
					break;
				case 9:
					System.out.println("Volver al menú principal");
					System.out.println(" ");
					break;
				default:
					System.out.println("*************************");
					System.out.println("* Opción no comprendida *");
					System.out.println("*************************");
					System.out.println(" ");
					break;
				}
			} while (opcion != 9);
		} else {
			System.out.println("Usuario o Contraseña erroneos");
			System.out.println(" ");
		}
	}

}
